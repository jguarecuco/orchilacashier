﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CoreOrchilaREST.Models;
using System.Net;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Security.Cryptography;
namespace CoreOrchilaREST
{
    /// <summary>
    /// 
    /// </summary>
    public class Utility
    {
        static string masterkey = "BO08072015";
        static betdevEntities db = new betdevEntities();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="postdata"></param>
        /// <returns></returns>
        public static string formData(Dictionary<string, string> postdata)
        {
            string stringdata = "";
            foreach (var item in postdata)
            {
                stringdata += item.Key + "=" + item.Value + "&";
            }

            return stringdata;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static session_integrator get_session(string token)
        {
            try{
            var session = db.session_integrator.Single(f => f.Token == token && f.Status == "A");
            return session;
                        }
            catch (Exception ex)
            {

                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static bo_form_register get_identification(session_integrator session)
        {
            try
            {
                
                var usuario = db.bo_form_register.Single(f => f.login == session.Username);
                return usuario;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static dux get_dux(string username)
        {
            try
            {

                var usuario = db.dux .Single(f => f.username ==  username);
                return usuario;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static decimal getSportBookBalance(string usuario,string password )
        {
            var recu = new ServiceApiParlay.ServiceApiWebClient();
            ServiceApiParlay.responseLogin respuesta = null;
            string[] cadena2 = { password };
            //emailregistro1("jguarecuco@gmail.com" , password);
            respuesta = recu.remoteLogin("f737b0db-2d1f-4bba-8efe-bc84b91287b8", usuario, md5crypto(cadena2));
            string token = respuesta.token;
            ServiceApiParlay.responseBalance balance = recu.getBalance(respuesta.token,respuesta.userId); 
            return decimal.Parse(balance.balance.ToString()) ;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static decimal getBetlaoPlus(string token)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("token", token );
            data.Add("masterkey", "BO08072015");
            string respuesta = Utility.HTTPPost(data, "http://www.betlaorchila.com:8898/api/userbalance");
            MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);
            return  rest.RESULT.UserBalance ;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cadena"></param>
        /// <returns></returns>
        public static string md5crypto(string[] cadena)
        {
            string input = "";
            foreach (var item in cadena)
            {
                input += item;
            }
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static decimal  getCasinoBalance(string username,string password){
            string urls= "https://cashier-cur.techonlinecorp.com/get_playerinfo.php?username=" + username + "&casino=inandplay&password=" + password + "&secretkey=ay3vb7iu5ty&responsetype=xml&excludedetails=1";
            WebClient wc = new WebClient();
            string string_rest=wc.DownloadString(urls);
            wc.Dispose();
            StringReader stringReader = new StringReader(string_rest);
            string g = "";
            System.Xml.Serialization.XmlSerializer reader =
      new System.Xml.Serialization.XmlSerializer(typeof(playerinfo));
            playerinfo datos = (playerinfo)reader.Deserialize(stringReader);
           

            return datos.balance;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="postdata"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string HTTPPost(Dictionary<string, string> postdata, string url)
        {
            var postData = formData(postdata);        
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData.ToString());
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            Console.WriteLine(responseFromServer);
            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public static void logSavedb(string request,string response)
        {
            var log = new rest_full();
            log.rest = request;
            log.md5 = response;
            log.ndate = DateTime.Now;
            db.rest_full.Add(log);
            db.SaveChanges(); 
        }

    }
}