﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreOrchilaREST.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MDLResultBalance
    {
        public string status { set; get; }
        public string Menssage { set; get; }
        public RestBalance result { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class RestBalance
    {
        public decimal main_balance { set; get; }
        public decimal casino_balance { set; get; }
        public decimal sportsbook_balance { set; get; }
        public decimal boplus_balance { set; get; }
        public decimal total_balance { set; get; }
    }
}