﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreOrchilaREST.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MDLResultProfile
    {
        public string status { set; get; }
        public string Menssage { set; get; }
        public RestProfile result { set; get; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class RestProfile
    {
        public string name { set; get; }
        public string second_name { set; get; }
        public string identification_id { set; get; }
        public string acount_emails { set; get; }
        public string username { set; get; }
        public DateTime birthday { set; get; }
    }
}