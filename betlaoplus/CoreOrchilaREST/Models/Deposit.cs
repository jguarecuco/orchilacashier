﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreOrchilaREST.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MDLDeposit
    {
        public string token { set; get; }
        public decimal amount { set; get; }
        public string currency { set; get; }
        public string bankname { set; get; }
        public string reference { set; get; }
        public DateTime deposit_date { set; get; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class MDLResultDeposit
    {
        public string status { set; get; }
        public string Menssage { set; get; }
        public dynamic result { set; get; }
    }

}