﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CoreOrchilaREST.Models;

namespace CoreOrchilaREST.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ProfilerController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public MDLResultProfile Post([FromBody] MDLRest_single model) {
            try
            {
                session_integrator session = Utility.get_session(model.token);
                bo_form_register usuario = Utility.get_identification(session);
                RestProfile profile = new RestProfile();
                profile.name = usuario.name;
                profile.second_name = usuario.apellido;
                profile.acount_emails = usuario.email;
                profile.username = usuario.login;
                profile.identification_id = usuario.ci;
                profile.birthday = usuario.bithday.Value ;
                MDLResultProfile resultado = new MDLResultProfile();
                resultado.status = "00";
                resultado.Menssage = "Success";
                resultado.result = profile;
                return resultado;
 
            }
            catch (Exception ex)
            {
                MDLResultProfile result = new MDLResultProfile();
                result.Menssage = "Sessión no valida";
                result.status = "01";
                result.result = new RestProfile();
                return result;
            }

        }
    }
}
/*
 * Status:
 * 00 - success
 * 01 - Sessión no valida
 * 02 - Moneda no Valida
 * 03 - Monto no valido
 * 04 - Nombre del Banco
 * 05 - Fecha Invalida
  */