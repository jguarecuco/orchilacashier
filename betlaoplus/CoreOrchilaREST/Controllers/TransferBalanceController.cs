﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CoreOrchilaREST.Models;
using System.IO;
namespace CoreOrchilaREST.Controllers
{
    /// <summary>
    /// Transferencias entre cajas
    /// </summary>
    public class TransferBalanceController : ApiController
    {
        betdevEntities db = new betdevEntities();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public MDLResult Post([FromBody]MDLTransfer model)
        {

            try
            {
                var session = Utility.get_session(model.token);
                var identidad = Utility.get_identification(session);
                var dux = Utility.get_dux(identidad.login);
                var token_orchila_plus = session.tokenblaoplus;
                var master_key = "BO08072015";
                switch (model.cashier_from)
                {
                    case 1:
                        switch (model.cashier_to)
                        {
                            case 2:
                                if (dux.TBETS < model.amount)
                                {
                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Credit Insufficient";
                                    result.status = "07";
                                    return result;
                                }
                                String serial = DateTime.Now.Ticks.ToString().Substring(0, 14);
                                string urls = "https://cashier-cur.techonlinecorp.com/instantcashdeposit.php?username=" + identidad.login + "&casino=inandplay&secretkey=kuaybv73yb&password=" + identidad.password + "&externaltranid=" + serial + "&amount=" + model.amount.ToString() + "&kioskcode=" + dux.kioscode + "&kioskadmincode=" + dux.kioskadmincode + "&responsetype=xml";
                                WebClient wc = new WebClient();

                                string string_rest = wc.DownloadString(urls);
                                Utility.logSavedb(urls, string_rest);
                                wc.Dispose();
                                StringReader stringReader = new StringReader(string_rest);

                                System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(instantcash));
                                instantcash datos = (instantcash)reader.Deserialize(stringReader);
                                if (datos.status == "approved")
                                {
                                    dux.TBETS -= model.amount;
                                    db.Entry(dux).State = System.Data.EntityState.Modified;
                                    db.SaveChanges();

                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Success";
                                    result.status = "00";
                                    return result;
                                }
                                else
                                {
                                    MDLResult result = new MDLResult();
                                    result.Menssage = datos.status;
                                    result.status = "06";
                                    return result;
                                }
                                break;
                            case 3:
                                string token_sport = session.tokensportbook;
                                if (dux.TBETS < model.amount)
                                {

                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Credit Insufficient";
                                    result.status = "07";
                                    return result;

                                }
                                ServiceApiParlay.ServiceApiWebClient sportbookapi = new ServiceApiParlay.ServiceApiWebClient();
                                ServiceApiParlay.responseLogin relogin = sportbookapi.keepAlive(token_sport);
                                if (!relogin.result)
                                {

                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Session Invalid";
                                    result.status = "01";
                                    return result;

                                }
                                ServiceApiParlay.responseCredit servicio = sportbookapi.addCredit(token_sport, session.user_id.Value, Double.Parse(model.amount.ToString()));
                                if (servicio.result)
                                {
                                    dux.TBETS -= model.amount;
                                    db.Entry(dux).State = System.Data.EntityState.Modified;
                                    db.SaveChanges();
                                    MDLResult result1 = new MDLResult();
                                    result1.Menssage = "Success";
                                    result1.status = "00";
                                    return result1;
                                }
                                else
                                {
                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Error";
                                    result.status = "08";
                                    return result;
                                }
                                break;
                            case 4:
                                Dictionary<string, string> data = new Dictionary<string, string>();
                                data.Add("token", token_orchila_plus);
                                data.Add("masterkey", "BO08072015");
                                data.Add("amount_transaction", model.amount.ToString());
                                data.Add("type_transaction", "110");
                                string respuesta = Utility.HTTPPost(data, "http://www.betlaorchila.com:8898/api/userdeposit");
                                MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);
                                if (dux.TBETS < model.amount)
                                {
                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Credit Insufficient";
                                    result.status = "07";
                                    return result;
                                }
                                if (rest.Response == "OK")
                                {
                                    dux.TBETS -= model.amount;
                                    db.Entry(dux).State = System.Data.EntityState.Modified;
                                    db.SaveChanges();

                                    MDLResult result = new MDLResult();
                                    result.Menssage = "SUCCESS";
                                    result.status = "00";
                                    return result;
                                }
                                else
                                {
                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Error";
                                    result.status = "08";
                                    return result;
                                }
                                 
                                 
                        }
                        break;
                    case 2:
                        decimal casino_balance = Utility.getCasinoBalance(dux.username, dux.passowrd_dux);
                        if (casino_balance < model.amount)
                        {
                            MDLResult result = new MDLResult();
                            result.Menssage = "Credit Insufficient";
                            result.status = "07";
                            return result;
                        }

                        String serial1 = DateTime.Now.Ticks.ToString().Substring(0, 14);
                        string urls1 = "https://cashier-cur.techonlinecorp.com/instantcashwithdraw.php?username=" + identidad.login + "&casino=inandplay&secretkey=kuaybv73yb&password=" + identidad.password + "&externaltranid=" + serial1 + "&amount=" + model.amount.ToString() + "&kioskcode=" + dux.kioscode + "&kioskadmincode=" + dux.kioskadmincode + "&responsetype=xml";
                        WebClient wc1 = new WebClient();

                        string string_rest1 = wc1.DownloadString(urls1);
                        Utility.logSavedb(urls1, string_rest1);
                        wc1.Dispose();

                        StringReader stringReader1 = new StringReader(string_rest1);

                        System.Xml.Serialization.XmlSerializer reader1 = new System.Xml.Serialization.XmlSerializer(typeof(instantcash));
                        instantcash datos1 = (instantcash)reader1.Deserialize(stringReader1);
                        if (datos1.status == "approved")
                        {
                            dux.TBETS += model.amount;
                            db.Entry(dux).State = System.Data.EntityState.Modified;
                            db.SaveChanges();
                            MDLResult result = new MDLResult();
                            result.Menssage = "Success";
                            result.status = "00";
                            return result;
                        }
                        else
                        {
                            MDLResult result = new MDLResult();
                            result.Menssage = datos1.status;
                            result.status = "06";
                            return result;
                        }

                        break;
                    case 3:
                        string token_sport1 = session.tokensportbook;
                        decimal balance_available = Utility.getSportBookBalance(dux.username, dux.passowrd_dux);
                        if (balance_available < model.amount)
                        {
                            MDLResult result = new MDLResult();
                            result.Menssage = "Credit Insufficient";
                            result.status = "07";
                            return result;
                        }
                        ServiceApiParlay.ServiceApiWebClient sportbookapi1 = new ServiceApiParlay.ServiceApiWebClient();
                        ServiceApiParlay.responseLogin relogin1 = sportbookapi1.keepAlive(token_sport1);
                        if (!relogin1.result)
                        {


                            MDLResult result = new MDLResult();
                            result.Menssage = "Session Invalid";
                            result.status = "01";
                            return result;

                        }
                        ServiceApiParlay.responseCredit servicio1 = sportbookapi1.remCredit(token_sport1, session.user_id.Value, Double.Parse(model.amount.ToString()));
                        if (servicio1.result)
                        {
                            dux.TBETS += model.amount;
                            db.Entry(dux).State = System.Data.EntityState.Modified;
                            db.SaveChanges();
                            MDLResult result1 = new MDLResult();
                            result1.Menssage = "Success";
                            result1.status = "00";
                            return result1;
                        }
                        else
                        {
                            MDLResult result = new MDLResult();
                            result.Menssage = "Error";
                            result.status = "08";
                            return result;
                        }
                        break;
                    case 4: 
                                 Dictionary<string, string> data1 = new Dictionary<string, string>();
                                data1.Add("token", token_orchila_plus);
                                data1.Add("masterkey", "BO08072015");
                                data1.Add("amount_transaction", model.amount.ToString());
                                data1.Add("type_transaction", "111");
                                string respuesta1 = Utility.HTTPPost(data1, "http://www.betlaorchila.com:8898/api/UserWithdraw");
                                MDLResponse rest1 = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta1);
                                if (Utility.getBetlaoPlus(session.tokenblaoplus) > model.amount)
                                {
                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Credit Insufficient";
                                    result.status = "07";
                                    return result;
                                }
                                if (rest1.Response == "OK")
                                {
                                    dux.TBETS += model.amount;
                                    db.Entry(dux).State = System.Data.EntityState.Modified;
                                    db.SaveChanges();

                                    MDLResult result = new MDLResult();
                                    result.Menssage = "SUCCESS";
                                    result.status = "00";
                                    return result;
                                }
                                else
                                {
                                    MDLResult result = new MDLResult();
                                    result.Menssage = "Error";
                                    result.status = "08";
                                    return result;
                                }
                        break;
                }

                MDLResult result2 = new MDLResult();
                result2.Menssage = "Success";
                result2.status = "00";
                return result2;
            }
            catch (Exception ex)
            {
                MDLResult result = new MDLResult();
                result.Menssage = "Sessión no valida";
                result.status = "01";
                return result;
            }


        }
    }
}
