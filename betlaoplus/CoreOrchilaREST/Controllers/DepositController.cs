﻿using CoreOrchilaREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CoreOrchilaREST.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class DepositController : ApiController
    {
        betdevEntities db = new betdevEntities();

        /// <summary>
        /// Metodo que se utiliza para realizar un depósito desde banco, requiere de la aprobación de un operador administrativo.
        /// </summary>
        /// <param name="model">
        /// token: Seria valor de la sesión emitido por betlaorchila.com
        /// amount: Monto del deposito, debe ser igual al que se emitio en el banco.
        /// deposit_date: Fecha en que se emitio el deposito o transferencia en el banco.
        /// bankname: Nombre de la entidad bancaria.
        /// reference: No. de documento emitido por el banco.
        /// currency: Moneda en uso(Bs. o USD)
        /// </param>
        /// <returns>
        /// status: estado del proceso enviado desde el servidor{00 - success,01 - Sessión no valida,02 - Moneda no Valida,03 - Monto no valido,04 - Nombre del Banco en blanco,05 - Fecha Invalida}
        /// Menssage: Mensaje desde el servidor.
        /// </returns>
        public MDLResult Post([FromBody] MDLDeposit model)
        {
            try
            {
                var response = Request.CreateResponse();
                response.StatusCode = HttpStatusCode.Accepted;
                depositos deposito = new depositos();
                var session = db.session_integrator.Single(f => f.Token == model.token && f.Status == "A");
                var usuario = db.bo_form_register.Single(f => f.login == session.Username);
                deposito.banco = model.bankname;
                deposito.fecha = DateTime.Now;
                deposito.fecha_deposito = model.deposit_date;
                deposito.moneda = model.currency;
                deposito.monto = model.amount;
                deposito.usuario = usuario.login;
                deposito.Estado = 0;
                deposito.caja = "";
                deposito.operador = 0;
                deposito.supervisor = 0;
                deposito.version = "2.0";
                deposito.transaccion = model.reference;
                db.depositos.Add(deposito);
                db.SaveChanges();
                transacciones transaccion = new transacciones();
                transaccion.fecha = deposito.fecha;
                transaccion.id_operacion = deposito.id;
                transaccion.monto = deposito.monto;
                transaccion.tipo = 2;
                transaccion.desde = "BANCO";
                transaccion.hasta = "CAJA PRINCIPAL";
                transaccion.usuario = usuario.login;
                transaccion.banco = model.bankname;
                db.transacciones.Add(transaccion );
                db.SaveChanges();
 
                MDLResult result = new MDLResult();
                result.status = "00";
                result.Menssage = "Success";
                return result;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse();
                response.StatusCode = HttpStatusCode.Unauthorized;
                MDLResult result = new MDLResult();
                result.status = "01";
                result.Menssage ="Sessión no valida.";
                return result;
            }
        }
    }
}
 