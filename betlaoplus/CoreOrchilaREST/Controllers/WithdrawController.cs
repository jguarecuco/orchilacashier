﻿using CoreOrchilaREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CoreOrchilaREST.Controllers
{
    public class WithdrawController : ApiController
    {
        betdevEntities db = new betdevEntities();
        public MDLResult Post([FromBody]MDLWithdraw model)
        {
            
            var session = db.session_integrator.Single(f => f.Token == model.token && f.Status == "A");
            var usuario = db.bo_form_register.Single(f => f.login == session.Username);
            var dux = Utility.get_dux(usuario.login);

            if (model.monto > dux.TBETS && model.monto<100)
            {
                MDLResult result1 = new MDLResult();
                result1.status = "10";
                result1.Menssage = "invalid amount";
                return result1;
            }
            
            retiros retiro = new retiros();
            retiro.usuario = session.Username;
            retiro.moneda = "Bs.";
            retiro.monto = model.monto;
            retiro.tipocuenta = model.tipo_cuenta;
            retiro.banco = model.banco;
            retiro.fecha = DateTime.Now;
            retiro.cedula = usuario.ci;
            retiro.cuenta = model.cuenta;
            retiro.aprobar = 0;
            retiro.operador = 0;
            retiro.saldo = 0;
            db.retiros.Add(retiro);
            db.SaveChanges();
            movientos_caja movimiento = new movientos_caja();
                    movimiento.descripcion = "Retiro de Saldo";
        movimiento.monto = model.monto;
        movimiento.saldo = 0;
        movimiento.tipo = "4";
        movimiento.usuaro = retiro.usuario;
        movimiento.fecha = DateTime.Now;
        movimiento.idmov = retiro.id;
        db.movientos_caja.Add(movimiento);
        db.SaveChanges();
        dux.TBETS -= model.monto;
                    db.Entry(dux).State = System.Data.EntityState.Modified;
                    db.SaveChanges();
            MDLResult result = new MDLResult();
            result.status = "00";
            result.Menssage = "Success";
            return result;
        }
    }
}
