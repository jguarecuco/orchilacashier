﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CoreOrchilaREST.Models;

namespace CoreOrchilaREST.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BalanceController : ApiController
    {
        betdevEntities db = new betdevEntities();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public MDLResultBalance Post([FromBody]MDLRest_single model)
        {
            try
            {
                session_integrator session = Utility.get_session(model.token);
                bo_form_register usuario = Utility.get_identification(session);
                dux userbalance = db.dux.Single(f => f.username == usuario.login);
                RestBalance balance = new RestBalance();
                balance.main_balance = userbalance.TBETS.Value;
                balance.casino_balance = Utility.getCasinoBalance(userbalance.username, userbalance.passowrd_dux);
                balance.sportsbook_balance = Utility.getSportBookBalance(userbalance.username, userbalance.passowrd_dux);
                balance.boplus_balance = Utility.getBetlaoPlus(session.tokenblaoplus);
                balance.total_balance = balance.main_balance + balance.sportsbook_balance + balance.casino_balance + balance.boplus_balance;
                MDLResultBalance resulta = new MDLResultBalance();
                resulta.status = "00";
                resulta.Menssage = "Success";
                resulta.result = balance;
                return resulta;
            }
            catch (Exception ex) {
                var response = Request.CreateResponse();
                response.StatusCode = HttpStatusCode.Unauthorized;
                MDLResultBalance result = new MDLResultBalance();
                result.status = "01";
                result.result = new RestBalance();
                result.Menssage = "Sessión no valida.";
                return result;
            }
        }
    }
}
