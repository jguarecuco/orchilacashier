﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<!DOCTYPE html>
<html class="html">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="7.0.314.244"/>
  <title>games</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<%=Url.Content("~/contenido/")  %>css/site_global.css?417434784"/>
  <link rel="stylesheet" type="text/css" href="<%=Url.Content("~/contenido/")  %>css/games.css?297015519" id="pagesheet"/>
  <!-- Other scripts -->
  <script type="text/javascript">
      document.documentElement.className += ' js';
      var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
     <script src="../../Contenido/scripts/jquery-1.8.3.min.js"></script>
  <script type="text/javascript">
      document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/actor:n4:all.js" type="text/javascript">\x3C/script>');
</script>
     <script type="text/javascript">
         function popitup(url) {
             newwindow = window.open(url, 'name', 'height=600,width=800');
             if (window.focus) { newwindow.focus() }
             return false;
         }
        </script>
            <style type="text/css">
        #llega
        {
            top: 332px;
            left: 432px;
            width: 334px;
        }
        .cerrar{
            width: 25px;
            height: 25px;
            top: 0px;
            left: 675px;
            background-color: #353535;
            background-image: url('/contenido/images/salir.png');
            background-repeat: no-repeat;
            background-position-x: 6px;
            background-position-y: 6px;
            border: 1px solid #353535;
            cursor: pointer;
            position: relative;
        }
        .cerrar:hover{
            border: 1px solid #3c3c3c;
        }
    </style>
   </head>
 <body>
    <div class="cerrar">

    </div>

    <script>
        $(".cerrar").click(function () {
            $("#u2109", window.parent.document).trigger("click")
        });
    </script>
  <div class="clearfix" id="page"><!-- column -->
   <div class="clearfix colelem" id="u2770-4"><!-- content -->
    <p><%:ViewBag.titulo %></p>
   </div>
   <div class="clearfix colelem" id="ppu2780-4"><!-- group -->
    <div class="clearfix grpelem" id="pu2780-4"><!-- column -->
     <div class="clearfix colelem" id="u2780-4"><!-- content -->
      <p><%:ViewBag.resumen %></p>
     </div>
     <div class="clearfix colelem" id="u2771-4"><!-- content -->
      <p>Jugado: <%:ViewBag.visitas %></p>
     </div>
     <div class="clearfix colelem" id="u2937-4"><!-- content -->
      <p></p>
     </div>
    </div>
    <div class="clip_frame grpelem" id="u2998"><!-- image -->
     <img class="block" id="u2998_img" src="<%=Url.Content("~/images/games/")  %><%=ViewBag.imagen1  %>" alt="" width="240" height="164"/>
    </div>
    <div class="clip_frame grpelem" id="u2993"><!-- image -->
     <img class="block" id="u2993_img" src="<%=Url.Content("~/images/games/")  %><%=ViewBag.imagen  %>" alt="" width="190" height="130"/>
    </div>
   </div>
   <div class="clearfix colelem" id="pbuttonu3029"><!-- group -->
    <a class="nonblock nontext Button rounded-corners clearfix grpelem" id="buttonu3029" href="javascript:void" onClick="popitup('http://cache.download.banner.flashinandplay.com/flash/19/casino_flashinandplay/[es]/help/index.html?hidetree=1&version=14.3.3.6&id=<%:ViewBag.codigo %>')"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u3031"><!-- group --><div class="rounded-corners clearfix grpelem" id="u3035"><!-- group --><div class="rounded-corners clearfix grpelem" id="u3036"><!-- group --><div class="rounded-corners clearfix grpelem" id="u3032"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u3030"><!-- group --><div class="clearfix grpelem" id="u3033-4"><!-- content --><p>Detalles</p></div></div></div></div></div></div></a>
    <a class="nonblock nontext Button rounded-corners clearfix grpelem" id="buttonu2772" href="javascript:void" onclick="popitup('http://cache.download.banner.flashinandplay.com/casinoclient.html?language=es&affiliates=1&nolobby&mode=offline&game=<%:ViewBag.codigo %>')"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u2778"><!-- group --><div class="rounded-corners clearfix grpelem" id="u2774"><!-- group --><div class="rounded-corners clearfix grpelem" id="u2777"><!-- group --><div class="rounded-corners clearfix grpelem" id="u2775"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u2779"><!-- group --><div class="clearfix grpelem" id="u2773-4"><!-- content --><p>Jugar</p></div></div></div></div></div></div></a>
   </div>
   <div class="verticalspacer"></div>
  </div>
  <!--[if lt IE 9]>
  <div class="preload_images">
   <img class="preload" src="images/u380-r-grad.png" alt=""/>
   <img class="preload" src="images/u380-grad.png" alt=""/>
  </div>
  <![endif]-->
  <!-- JS includes -->
  <script src="<%=Url.Content("~/contenido/")  %>scripts/museutils.js?3865766194" type="text/javascript"></script>
  <script src="<%=Url.Content("~/contenido/")  %>scripts/jquery.watch.js?4068933136" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
      $(document).ready(function () {
          try {
              Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
              Muse.Utils.prepHyperlinks(true);/* body */
              Muse.Utils.fullPage('#page');/* 100% height page */
              Muse.Utils.showWidgetsWhenReady();/* body */
              Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
          } catch (e) { Muse.Assert.fail('Error calling selector function:' + e); }
      });
</script>
   </body>
</html>
