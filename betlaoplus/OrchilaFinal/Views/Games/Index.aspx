﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<OrchilaFinal.Games>>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Index</title>
    <style>
        html, body {
            background:none;
            background-color:none;
        }
        ul{
            list-style:none;
            list-style-image:none;
            list-style-position:initial;
            list-style-type:none;
            text-decoration:none;

        }
        .thumbs{
             float:left;
             width:170px;
             height:115px;
             margin:5px;
            }

    </style>
    <script src="../../Contenido/scripts/jquery-1.8.3.min.js"></script>
</head>
<body>
    <ul>
    <%foreach(var item in Model){ %>
        <li class="thumbs <%:item.thumbs.Replace(" ","_") %>"><a href="javascript:void" id="<%:item.codigo %>"><img src="<%:Url.Content("~/images/games/") %><%:item.imagen1 %>" style="width:170px;height:115px;" /></a></li>
      <script>
          $("#<%: item.codigo%>").click(function () {
              var body = document.body,
              html = document.documentElement;
              var height = Math.max(body.scrollHeight, body.offsetHeight,
              html.clientHeight, html.scrollHeight, html.offsetHeight);
              $('#u2114', window.parent.document).trigger('click');
			  $('#u2110', window.parent.document).css('width','717px');
			  $('#u2110', window.parent.document).css('height','457px');
			  $('#u2111', window.parent.document).css('width','703px');
			  $('#u2111', window.parent.document).css('height','440px');
			  
			  $('#games', window.parent.document).attr("width","703px");
			  $('#games', window.parent.document).attr("height","440px");
              $('#games', window.parent.document).attr('src', '<%:Url.Content("~/games/game/")%><%:item.codigo %>')
          });
      </script>
        
    <% }%>
    </ul>
      
</body>
</html>
