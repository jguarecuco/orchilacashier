﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<!DOCTYPE html>
<html class="html">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="7.0.314.244"/>
  <title>listo</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Contenido/") %>css/site_global.css?417434784"/>
  <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Contenido/") %>css/listo.css?463350483" id="pagesheet"/>
  <!-- Other scripts -->
<script src="../../Contenido/scripts/jquery-1.8.3.min.js"></script>
  <script type="text/javascript">
      document.documentElement.className += ' js';
</script>
     <style>
        .cerrar{
            width: 25px;
            height: 25px;
            top: 1px;
            left: 760px;
            background-color: #353535;
            background-image: url('/contenido/images/salir.png');
            background-repeat: no-repeat;
            background-position-x: 6px;
            background-position-y: 6px;
            border: 1px solid #353535;
            cursor: pointer;
            position: relative;
        }
        .cerrar:hover{
            border: 1px solid #3c3c3c;
        }
     </style>
   </head>
 <body>
    <div class="cerrar">

    </div>

    <script>
        $(".cerrar").click(function () {
            $("#u1049", window.parent.document).trigger("click")
        });
    </script>

  <div class="clearfix" id="page"><!-- column -->
   <div class="clearfix colelem" id="u3542"><!-- group -->
    <div class="clip_frame grpelem" id="u3537"><!-- image -->
     <img class="block" id="u3537_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-capa%202.jpg" alt="" width="790" height="400"/>
    </div>
    <div class="clip_frame grpelem" id="u3532"><!-- image -->
     <img class="block" id="u3532_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-rect%c3%a1ngulo%201.png" alt="" width="790" height="400"/>
    </div>
    <div class="clip_frame grpelem" id="u3527"><!-- image -->
     <img class="block" id="u3527_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-capa%204.png" alt="" width="790" height="400"/>
    </div>
    <div class="clip_frame grpelem" id="u3522"><!-- image -->
     <img class="block" id="u3522_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-capa%203.png" alt="" width="790" height="400"/>
    </div>
    <div class="clip_frame grpelem" id="u3517"><!-- image -->
     <img class="block" id="u3517_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-%c2%a1tu%20registro%20se%20ha%20completado%20exitosamente!.png" alt="" width="790" height="400"/>
    </div>
    <div class="clip_frame grpelem" id="u3512"><!-- image -->
     <img class="block" id="u3512_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-en%20pocos%20minutos%20estar%c3%a1s%20recibiendo%20un%20correo%20electr%c3%b3nico%20con%20.png" alt="" width="790" height="400"/>
    </div>
    <div class="clip_frame grpelem" id="u3507"><!-- image -->
     <img class="block" id="u3507_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-forma%201.png" alt="" width="790" height="400"/>
    </div>
    <div class="clip_frame grpelem" id="u3502"><!-- image -->
     <img class="block" id="u3502_img" src="<%: Url.Content("~/Contenido/") %>images/confirmacion%20de%20regristro-capa%206.png" alt="" width="790" height="400"/>
    </div>
   </div>
   <div class="verticalspacer"></div>
  </div>
  <!-- JS includes -->
 
  <script src="<%: Url.Content("~/Contenido/") %>scripts/museutils.js?3865766194" type="text/javascript"></script>
  <script src="<%: Url.Content("~/Contenido/") %>scripts/jquery.watch.js?4068933136" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
      $(document).ready(function () {
          try {
              Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
              Muse.Utils.prepHyperlinks(true);/* body */
              Muse.Utils.fullPage('#page');/* 100% height page */
              Muse.Utils.showWidgetsWhenReady();/* body */
              Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
          } catch (e) { Muse.Assert.fail('Error calling selector function:' + e); }
      });
</script>
   </body>
</html>
