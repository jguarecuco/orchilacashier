﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<OrchilaFinal.Models.Registro>" %>

<html lang="es-ve">
<head id="Head1" runat="server">
    <title>Registros</title>


    <%--    <link href="../../css/dark-hive/jquery-ui-1.10.4.custom.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../Content/terminos.css" rel="stylesheet" type="text/css" />
    <link href="../../Content/formulario.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../../js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../../js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../../js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

    <link href="../../css/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="../../css/base/demos.css" rel="stylesheet" type="text/css" />

    <script src="../../js/jquery.datepicker.es.js" type="text/javascript"></script>
    <script>
        $(function () {
            $("#bithday").datepicker({
                showOn: "button",
                changeYear: true,
                yearRange: "1920:1996",
                buttonImage: "<%: Url.Content("~/images/boton-calendar.png") %>",
                    dateFormat: 'dd/mm/yy',
                    buttonImageOnly: true
                });
            });
    </script>
    <script type="text/javascript">
        function popitup(url) {
            newwindow = window.open(url, 'name', 'height=600,width=800');
            if (window.focus) { newwindow.focus() }
            return false;
        }
    </script>
    <style type="text/css">
        #llega {
            top: 332px;
            left: 432px;
            width: 334px;
        }

        .cerrar {
            width: 25px;
            height: 25px;
            top: 1px;
            left: 760px;
            background-color: #353535;
            background-image: url('/contenido/images/salir.png');
            background-repeat: no-repeat;
            background-position-x: 6px;
            background-position-y: 6px;
            border: 1px solid #353535;
            cursor: pointer;
            position: relative;
        }

            .cerrar:hover {
                border: 1px solid #3c3c3c;
            }
    </style>
</head>
<body bgcolor="#000000">
    <div class="cerrar">
    </div>

    <script>
        $(".cerrar").click(function () {
            $("#u1049", window.parent.document).trigger("click")
        });
    </script>
    <% using (Html.BeginForm( ))
           { %>
    <%try
          {
          if (Html.ValidationSummary(true).ToString( ).Count( ) > 0)
              {
    %>
    <script>
        alert("<%:ViewBag.msgerror%>");
    </script>
    <% 
            }
      }
      catch (Exception e)
          {
          }
    %>
    <fieldset>
        <label class=" Formulario_de_registro">Formulario de Registro </label>
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="style1">
                    <label class="Nombre_text">Nombre</label>
                </td>
                <td class="style2">

                    <%: Html.TextBoxFor(model=> model.name, new  {@placeholder = "Escriba su nombre..",@class="nombre_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.name) %>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="apellido_text">Apellido</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor(model=> model.apellido, new {@placeholder = "Escriba su apellido..",@class="apellido_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.apellido)%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="Email_text">Correo Electronico</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor(model=> model.email , new {@placeholder = "nombreusuario@dominio.com",@class="email_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.email )%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="Email_text1">Confirmar Correo</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor(model=> model.resultado , new {@placeholder = "nombreusuario@dominio.com",@class="email_input1"})%>
                    <%: Html.ValidationMessageFor(model=> model.resultado)%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="cedula_text">Cédula de Identidad</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor(model=> model.ci, new {@placeholder = "Ejemplo: 13082875",@class="cedula_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.ci)%>
                </td>

            </tr>
            <tr>
                <td class="style1">
                    <label class="Fechan_text" id="campofecha">Fecha de Nacimiento</label>
                    <%--<input type="button" class="boton_calendar" />--%>
                </td>
                <td class="style1">
                    <%: Html.TextBoxFor(model=> model.bithday  , new {@placeholder = "Ejemplo: dd/mm/aaaa",@class="dia_input",@readonly="readonly"})%>
                    <%: Html.ValidationMessageFor(model=> model.otrareferencia )%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="pais_text">País</label>
                </td>
                <td class="input">
                    <%
       string[] pais = new string[1] { "Venezuela" };
                         
                    %>
                    <%: Html.DropDownListFor(model=> model.pais, new SelectList(pais), new{@placeholder = "Ejemplo: 13082875",@class="pais_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.bithday )%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="estado_text">Estado</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor(model=> model.estado, new {@placeholder = "Escriba su Estado", @class = "estado_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.estado )%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="telefono_text">Teléfono</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor (model=> model.telefono , new {@placeholder = "Numero Teléfonico",@class="telefono_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.telefono  )%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="pregunta_text">Pregunta Secreta</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor (model=> model.pregunta , new {@placeholder = "Pregunta secreta",@class="pregunta_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.pregunta)%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="respuesta_text">Respuesta Secreta</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor (model=> model.respuesta  ,new {@placeholder = "Respuesta secreta",@class="respuesta_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.pregunta)%>
                </td>
            </tr>

            <tr>
                <td class="form-input-name">
                    <label class="como_text">Como se Entero</label>
                </td>
                <td class="input">
                    <%
       string[] como = new string[] { "Facebook" , "Twitter" , "SMS" , "Un amigo" , "Otros" };
                             
                    %>
                    <%: Html.DropDownListFor(model=> model.entero, new SelectList(como), new {@placeholder = "Ejemplo: 13082875", @class = "como_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.entero)%>
                </td>
            </tr>
            <tr>
                <td class="form-input-name">
                    <label class="Referido_text">Referidos</label>
                </td>
                <td class="input">
                    <%: Html.TextBoxFor(model=> model.referencia, new {@placeholder = "Ejemplo: 13082875", @class = "referido_input"})%>
                    <%: Html.ValidationMessageFor(model=> model.referencia)%>
                </td>
            </tr>
            <tr>
                <td class="input" colspan="2">
                    <center>
                          <input id="term" name ="term"  checked type="checkbox" class="acepto_input" 
                            style="background-color: #000099;" /><label class="acepto_txt">Soy mayor de edad y acepto los <a href="javascript:void" onClick="popitup('../terminos/index.html')">Términos y condiciones</a></label> 
                        </center>
                </td>
            </tr>
            <tr>
                <td class="form-input-name"></td>
                <td class="input">
                    <a id="enviar" href="javascript:void" class="boton_input"></a>

                    <script>

                    </script>
                    <label
                        class="acepto_txt2" id="llega"
                        style="position: absolute; color: #FFFFFF; font-size: small;">
                    </label>
                </td>
            </tr>
        </table>
    </fieldset>
    <% }%>

    <script>
        $("#ci").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (e.target.value.indexOf(".") >= -1 && e.keyCode == 190) {
                e.preventDefault();
                return;
            }

            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $("#enviar").click(function () {

            var bandera = false;
            var valores = $("input");
            valores.each(function (a, b) {
                if ($(b).val().trim() == "") {
                    if (b.id != "referencia") {
                        alert("Debe rellenar todos los campos del Formulario");
                        bandera = true;
                        return false;
                    }
                }

            });
            if (bandera) {

                return false;
            }
            if ($("#name").val().trim() == "") {

            }

            if (!$("#term").prop("checked")) {
                alert("Debe Aceptar los terminos y condiciones.");
                return;
            }
            if ($("#email").val() == $("#resultado").val() & bandera == false) {
                $("#enviar").unbind("click");
                $("form").submit();
            } else {
                alert("Debe confirmar su correo electrónico.");
            }

        });
    </script>
</body>
</html>
