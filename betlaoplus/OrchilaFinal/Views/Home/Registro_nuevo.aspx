﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<OrchilaFinal.Models.MDLRegister>" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0">
    <title>index</title>
    <link rel="stylesheet" href="<%: Url.Content("~/registro/") %>css/standardize.css">
    <link rel="stylesheet" href="<%: Url.Content("~/registro/") %>css/index-grid.css">
    <link rel="stylesheet" href="<%: Url.Content("~/registro/") %>css/index.css">
    <link href="../../Registro/css/DatePickerCssbob.css" rel="stylesheet" />
    <link href="../../css/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <script src="../../Registro/js/jquery-min.js"></script>
    <script src="../../js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../../js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../../js/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../../js/jquery.datepicker.es.js" type="text/javascript"></script>

    <script src="../../Registro/js/RegisterScript.js"></script>
</head>
<body class="body page-index clearfix">

    
    <form method="post" id="registrar" autocomplete="off">
        <p class="text text-1">Formulario de registro</p>
        <button id="button-close" class="_button _button-1" type="button">x</button>
        <div class="errors alert alert-danger"><%try
          {
 
    %>

    <%:ViewBag.msgerror%>
    
    <% 
             
          }
      catch (Exception e)
          {
          }
    %></div>
        <p class="text text-2">Nombre</p>
        <p class="text text-3">Apellido</p>
        <p class="text text-4">Pregunta Secreta</p>
        <input id="name" data-id=1 class="_input _input-1" name="Name" type="text" value="<%:Model.Name %>">
        <input id="Apellido" data-id=2 class="_input _input-2" name="Apellido" type="text"  value="<%:Model.Apellido %>">
        <input id="Pregunta" data-id=9 class="_input _input-3" name="Pregunta" type="text"  value="<%:Model.Pregunta %>">
        <p class="text text-5">Correo Electronico</p>
        <p class="text text-6">Confirmar Correo</p>
        <p class="text text-7">Respuesta Secreta</p>
        <input id="Email" data-id=3 class="_input _input-4" name="Email" type="text"  value="<%:Model.Email %>">
        <input id="Email_confirm" data-id=4 class="_input _input-5" name="Email_confirm" type="text"  value="<%:Model.Email_confirm %>">
        <input id="Respuesta" data-id=10 class="_input _input-6" name="Respuesta" type="text"  value="<%:Model.Respuesta %>">
        <p class="text text-8">Cedula de Identidad</p>
        <p class="text text-9">Como se Entero</p>
        <input id="Cedula" data-id=5 class="_input _input-7" name="Cedula" type="text"  value="<%:Model.Cedula %>">
        <select id="Entero" class="_select _select-1" name="Entero">
            <option value="" selected>Facebook</option>
            <option value="Option">Twitter</option>
            <option value="Option">SMS</option>
            <option value="Option">Un amigo</option>
            <option value="Option">Otros</option>
        </select>
        <p class="text text-10">Fecha de Nacimiento</p>
        <p class="text text-11">Referidos</p>
        <p class="text text-12">R</p>
        <input id="Fecha" data-id=6 class="_input _input-8 datepicker ll-skin-latoja" name="Fecha" type="text" readonly="readonly" >
        <div class="element" id="botonBobCalendar"></div>
        <input id="Referidos" class="_input _input-9" name="Referidos" type="text"  value="<%:Model.Referidos %>">
        <p class="text text-13">País</p>
        <p class="text text-14">Estado</p>
        <select id="Pais" class="_select _select-2" name="Pais" >
            <option value="venezuela" selected>Venezuela</option>
        </select>
        <input id="Estado" data-id=7 class="_input _input-10" name="Estado" type="text"  value="<%:Model.Estado %>">
        <label class="checkbox-label clearfix">
            <input data-id=11 id="EsMayor" class="checkbox" name="EsMayor" type="checkbox">
            <span class="point-text">Soy mayor de edad y Acepto los</span>
            <p onclick="window.location='#';" class="text">Términos y condiciones</p>
        </label>
        <p class="text text-16">Teléfono</p>
        <input id="Telefono" data-id=8 class="_input _input-11" name="Telefono" type="text"  value="<%:Model.Telefono %>">
        <button id="button-enviar" class="_button _button-2" type="submit">Enviar Registro</button>
    </form>


</body>
</html>
