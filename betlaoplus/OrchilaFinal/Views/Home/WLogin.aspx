﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<OrchilaFinal.Models.Login>" %>
<!DOCTYPE html>
<html class="html">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="7.0.314.244"/>
  <title>login</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Contenido/") %>css/site_global.css?417434784"/>
  <link rel="stylesheet" type="text/css" href="<%: Url.Content("~/Contenido/") %>css/login.css?455417351" id="pagesheet"/>
  <!-- Other scripts -->
  <script type="text/javascript">
      document.documentElement.className += ' js';
      var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
<script src="../../Contenido/scripts/jquery-1.8.3.min.js"></script>
  <script type="text/javascript">
      document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/actor:n4:all.js" type="text/javascript">\x3C/script>');
</script>
     <style>
        .cerrar{
            width: 25px;
            height: 25px;
            top: 1px;
            left: 760px;
            background-color: #353535;
            background-image: url('/contenido/images/salir.png');
            background-repeat: no-repeat;
            background-position-x: 6px;
            background-position-y: 6px;
            border: 1px solid #353535;
            cursor: pointer;
            position: relative;
        }
        .cerrar:hover{
            border: 1px solid #3c3c3c;
        }
     </style>
   </head>
 <body>
    <div class="cerrar">

    </div>

    <script>
        $(".cerrar").click(function () {
            $("#u1049", window.parent.document).trigger("click")
        });
    </script>
  <div class="clearfix" id="page"><!-- group -->
   <div class="clearfix grpelem" id="pu3562"><!-- column -->
    <div class="museBGSize colelem" id="u3562"><!-- simple frame --></div>
    <div class="clearfix colelem" id="u3563-6"><!-- content -->
     <p id="u3563-2">Debes ingresar en tu cuenta para poder</p>
     <p id="u3563-4">entrar en esta sección</p>
    </div>
    <a class="nonblock nontext colelem" id="u3577" href="registro"><!-- simple frame --></a>
   </div>
   <div class="clearfix grpelem" id="pu3568-4"><!-- column -->
    <div class="clearfix colelem" id="u3568-4"><!-- content -->
     <p>Puedes iniciar sesión desde aquí</p>
    </div>
    <div class="clearfix colelem" id="u3565-4"><!-- content -->
     <p>Usuario</p>
    </div>
    <div class="colelem" id="u3564"><input type="text" placeholder="Ingrese su usuario" id="username" name="username" style="width:90%;padding:5px;" /></div>
    <div class="clearfix colelem" id="u3567-4"><!-- content -->
     <p>Contraseña</p>
    </div>
    <div class="colelem" id="u3566"><!-- simple frame --><input type="password" placeholder="Ingrese su Contraseña" id="password" name="password"  style="width:90%;padding:5px;" /></div>
    <a class="nonblock nontext Button rounded-corners clearfix colelem" id="buttonu3569" href="javascript:void"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u3573"><!-- group --><div class="rounded-corners clearfix grpelem" id="u3571"><!-- group --><div class="rounded-corners clearfix grpelem" id="u3570"><!-- group --><div class="rounded-corners clearfix grpelem" id="u3575"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u3574"><!-- group --><div class="clearfix grpelem" id="u3576-4"><!-- content --><p>Enviar</p></div></div></div></div></div></div></a>
   </div>
   <div class="verticalspacer"></div>
  </div>
  <!--[if lt IE 9]>
  <div class="preload_images">
   <img class="preload" src="images/u1096-r-grad.png" alt=""/>
   <img class="preload" src="images/u1096-grad.png" alt=""/>
  </div>
  <![endif]-->
  <!-- JS includes -->
 
  <script src="<%: Url.Content("~/Contenido/") %>scripts/museutils.js?3865766194" type="text/javascript"></script>
  <script src="<%: Url.Content("~/Contenido/") %>scripts/jquery.musepolyfill.bgsize.js?291134478" type="text/javascript"></script>
  <script src="<%: Url.Content("~/Contenido/") %>scripts/jquery.watch.js?4068933136" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
      $(document).ready(function () {
          try {
              Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
              Muse.Utils.prepHyperlinks(true);/* body */
              Muse.Utils.fullPage('#page');/* 100% height page */
              Muse.Utils.showWidgetsWhenReady();/* body */
              Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
          } catch (e) { Muse.Assert.fail('Error calling selector function:' + e); }
      });
</script>
     <script>
         $("#username").keypress(function (event) {
             if (event.which == 13) {
                  
                $("#password").focus();
            }

        });
         $("#password").keypress(function (event) {
             if (event.which == 13) {
                 $("#buttonu3569").trigger("click");
             }
         });
         $("#buttonu3569").click(function () {
             var request = $.ajax({
                 url: "<%: Url.Content("~/home/login") %>",
                 type: "POST",
                 data: {
                     username: $("#username").val().toUpperCase(), password: $("#password").val()
                 }
             });
             request.done(function (msg) {
                 if (msg == "OK") {
                     //window.location = "<%:Url.Content("~/")%>";
                     $("#u3165", window.parent.document).css("display", "block");
                     $("#u3129", window.parent.document).css("display", "none");
                     $("#u3164-4", window.parent.document).html("Bienvenidos Usuario(a) " + $("#username").val().toUpperCase());
                     parent.username = $("#username").val().toUpperCase();
                     parent.password = $("#password").val();
                     $("#username", window.parent.document).val("");
                     $("#password", window.parent.document).val("");
                     estado = true;
                     parent.estado = true;
                     $("#u1049", window.parent.document).trigger("click");
					 $("#u875", window.parent.document).unbind("click")
                 } else {
                     alert(msg);
                 }
             });
             request.fail(function (jqXHR, textStatus) {
                 alert("Request failed: " + textStatus);
             });
         });
     </script>
   </body>
</html>
