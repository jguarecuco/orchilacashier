﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html class="html">
 <head>

  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="7.2.232.244"/>
  <title>Medios</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="<%:Url.Content("~/medios/") %>css/site_global.css?417434784"/>
  <link rel="stylesheet" type="text/css" href="<%:Url.Content("~/medios/") %>css/master_a-p_gina-maestra.css?4217930870"/>
  <link rel="stylesheet" type="text/css" href="<%:Url.Content("~/medios/") %>css/index.css?3760332367" id="pagesheet"/>
  <!-- Other scripts -->
<script src="../../medios/scripts/jquery-1.8.3.min.js"></script>
  <script type="text/javascript">
      document.documentElement.className += ' js';
      var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
  <script type="text/javascript">
      document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/actor:n4:all;asap:n7:all.js" type="text/javascript">\x3C/script>');
</script>
   </head>
 <body>

  <div class="clearfix" id="page"><!-- column -->
   <div class="clearfix colelem" id="pu5732-3"><!-- group -->
    <div class="clearfix grpelem" id="u5732-3"><!-- content -->
     <p>&nbsp;</p>
    </div>
    <div class="clearfix grpelem" id="u5733-4"><!-- content -->
     <p>Medios</p>
    </div>
   </div>
   <div class="PamphletWidget clearfix colelem" id="pamphletu5640"><!-- none box -->
    <div class="ThumbGroup clearfix grpelem" id="u5725"><!-- none box -->
     <div class="popup_anchor">
      <div class="Thumb popup_element museBGSize" id="u5727"><!-- simple frame --></div>
     </div>
     <div class="popup_anchor">
      <div class="Thumb popup_element museBGSize" id="u5731"><!-- simple frame --></div>
     </div>
     <div class="popup_anchor">
      <div class="Thumb popup_element museBGSize" id="u5729"><!-- simple frame --></div>
     </div>
     <div class="popup_anchor">
      <div class="Thumb popup_element museBGSize" id="u5726"><!-- simple frame --></div>
     </div>
     <div class="popup_anchor">
      <div class="Thumb popup_element" id="u5730"><!-- simple frame --></div>
     </div>
     <div class="popup_anchor">
      <div class="Thumb popup_element museBGSize" id="u5728"><!-- simple frame --></div>
     </div>
    </div>
    <div class="popup_anchor" id="u5641popup">
     <div class="ContainerGroup clearfix" id="u5641"><!-- stack box -->
      <div class="Container clearfix grpelem" id="u5642"><!-- group -->
       <div class="museBGSize grpelem" id="u5643"><!-- simple frame --></div>
       <div class="clearfix grpelem" id="pu5644-4"><!-- column -->
        <div class="clearfix colelem" id="u5644-4"><!-- content -->
         <p>Brazuca, balón de la final del Mundial.</p>
        </div>
        <div class="clearfix colelem" id="u5645-4"><!-- content -->
         <p>La Fifa divulgó en su portal de internet las imágenes del Brazuca Final Rio, el balón que será usado en la final del Mundial Brasil&#45;2014...</p>
        </div>
        <a class="nonblock nontext Button rounded-corners clearfix colelem" id="buttonu5646" href="brazuca.html"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u5653"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5650"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5647"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5648"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u5652"><!-- group --><div class="clearfix grpelem" id="u5651-4"><!-- content --><p>Ver más</p></div></div></div></div></div></div></a>
       </div>
      </div>
      <div class="Container invi clearfix grpelem" id="u5690"><!-- group -->
       <div class="museBGSize grpelem" id="u5691"><!-- simple frame --></div>
       <div class="clearfix grpelem" id="pu5692-4"><!-- column -->
        <div class="clearfix colelem" id="u5692-4"><!-- content -->
         <p>Cristiano y Messi, las máximas figuras de Brasil&#45;2014</p>
        </div>
        <div class="clearfix colelem" id="u5701-4"><!-- content -->
         <p>Leo Messi y Cristiano Ronaldo son dos de los futbolistas que aspiran a encontrar el respaldo definitivo que les asiente en el olimpo...</p>
        </div>
        <a class="nonblock nontext Button rounded-corners clearfix colelem" id="buttonu5693" href="cristianomessi.html"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u5700"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5698"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5697"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5696"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u5699"><!-- group --><div class="clearfix grpelem" id="u5695-4"><!-- content --><p>Ver más</p></div></div></div></div></div></div></a>
       </div>
      </div>
      <div class="Container invi clearfix grpelem" id="u5702"><!-- group -->
       <div class="museBGSize grpelem" id="u5711"><!-- simple frame --></div>
       <div class="clearfix grpelem" id="pu5713-4"><!-- column -->
        <div class="clearfix colelem" id="u5713-4"><!-- content -->
         <p>Sedes del Mundial 2014.</p>
        </div>
        <div class="clearfix colelem" id="u5712-4"><!-- content -->
         <p>Las ciudades de Rio de Janeiro, Sao Paulo, Belo Horizonte, Porto Alegre, Brasilia, Curitiba...</p>
        </div>
        <a class="nonblock nontext Button rounded-corners clearfix colelem" id="buttonu5703" href="sedes.html"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u5704"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5708"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5709"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5705"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u5710"><!-- group --><div class="clearfix grpelem" id="u5707-4"><!-- content --><p>Ver más</p></div></div></div></div></div></div></a>
       </div>
      </div>
      <div class="Container invi clearfix grpelem" id="u5678"><!-- group -->
       <div class="museBGSize grpelem" id="u5681"><!-- simple frame --></div>
       <div class="clearfix grpelem" id="pu5680-5"><!-- column -->
        <div class="clearfix colelem" id="u5680-5"><!-- content -->
         <p id="u5680-2">Juegos de Estilo Clásico.</p>
         <p>&nbsp;</p>
        </div>
        <div class="clearfix colelem" id="u5679-4"><!-- content -->
         <p>Después de estar familiarizado con las características y botones de los juegos de estilo clásico es muy fácil entender la metodología de...</p>
        </div>
        <a class="nonblock nontext Button rounded-corners clearfix colelem" id="buttonu5682" href="clasico.html"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u5687"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5683"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5684"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5686"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u5688"><!-- group --><div class="clearfix grpelem" id="u5685-4"><!-- content --><p>Ver más</p></div></div></div></div></div></div></a>
       </div>
      </div>
      <div class="Container invi clearfix grpelem" id="u5666"><!-- group -->
       <div class="museBGSize grpelem" id="u5667"><!-- simple frame --></div>
       <div class="clearfix grpelem" id="pu5668-6"><!-- column -->
        <div class="clearfix colelem" id="u5668-6"><!-- content -->
         <p>Como jugar a</p>
         <p>la Ruleta</p>
        </div>
        <div class="clearfix colelem" id="u5669-4"><!-- content -->
         <p>Su nombre viene del término francés roulette, que significa &quot;rueda pequeña&quot;. La ruleta es típica de los casinos. Resulta uno de los juegos más...</p>
        </div>
        <a class="nonblock nontext Button rounded-corners clearfix colelem" id="buttonu5670" href="ruleta.html"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u5673"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5671"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5677"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5674"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u5676"><!-- group --><div class="clearfix grpelem" id="u5672-4"><!-- content --><p>Ver más</p></div></div></div></div></div></div></a>
       </div>
      </div>
      <div class="Container invi clearfix grpelem" id="u5654"><!-- group -->
       <div class="museBGSize grpelem" id="u5655"><!-- simple frame --></div>
       <div class="clearfix grpelem" id="pu5664-6"><!-- column -->
        <div class="clearfix colelem" id="u5664-6"><!-- content -->
         <p>Los más rápidos</p>
         <p>del Mundial.</p>
        </div>
        <div class="clearfix colelem" id="u5665-4"><!-- content -->
         <p>FIFA dio a conocer el Top Ten de los jugadores más rápidos del Mundo en el que figuran los dos últimos...</p>
        </div>
        <a class="nonblock nontext Button rounded-corners clearfix colelem" id="buttonu5656" href="rapidos.html"><!-- container box --><div class="rounded-corners clearfix grpelem" id="u5663"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5658"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5657"><!-- group --><div class="rounded-corners clearfix grpelem" id="u5661"><!-- group --><div class="gradient rounded-corners clearfix grpelem" id="u5660"><!-- group --><div class="clearfix grpelem" id="u5662-4"><!-- content --><p>Ver más</p></div></div></div></div></div></div></a>
       </div>
      </div>
     </div>
    </div>
   </div>
   <div class="clearfix colelem" id="pppu667"><!-- group -->
    <div class="clearfix grpelem" id="ppu667"><!-- group -->
     <div class="grpelem" id="pu667"><!-- inclusion -->
      <a class="nonblock nontext" id="u667" href="descartados.html"><!-- simple frame --></a>
      <div class="clearfix" id="pu668-6"><!-- group -->
       <a class="nonblock nontext clearfix grpelem" id="u668-6" href="descartados.html"><!-- content --><p>Los grandes “descartados”</p><p>del Mundial.</p></a>
      </div>
     </div>
     <a class="nonblock nontext museBGSize clip_frame grpelem" id="u665" href="descartados.html"><!-- image --></a>
    </div>
    <div class="grpelem" id="pu663"><!-- inclusion -->
     <a class="nonblock nontext" id="u663" href="gruposmundial.html"><!-- simple frame --></a>
     <div class="clearfix" id="pu664-4"><!-- group -->
      <a class="nonblock nontext clearfix grpelem" id="u664-4" href="gruposmundial.html"><!-- content --><p>Una constelación de 736 estrellas.</p></a>
     </div>
    </div>
    <div class="grpelem" id="pu672"><!-- inclusion -->
     <a class="nonblock nontext" id="u672" href="baccarat.html"><!-- simple frame --></a>
     <div class="clearfix" id="pu673-4"><!-- group -->
      <a class="nonblock nontext clearfix grpelem" id="u673-4" href="baccarat.html"><!-- content --><p>Como jugar Baccarat.</p></a>
     </div>
    </div>
    <a class="nonblock nontext museBGSize clip_frame grpelem" id="u670" href="baccarat.html"><!-- image --></a>
    <a class="nonblock nontext museBGSize clip_frame grpelem" id="u3206" href="gruposmundial.html"><!-- image --></a>
   </div>
   <div class="clearfix colelem" id="pppu681"><!-- group -->
    <div class="clearfix grpelem" id="ppu681"><!-- group -->
     <div class="grpelem" id="pu681"><!-- inclusion -->
      <a class="nonblock nontext" id="u681" href="fuleco.html"><!-- simple frame --></a>
      <div class="clearfix" id="pu682-6"><!-- group -->
       <a class="nonblock nontext clearfix grpelem" id="u682-6" href="fuleco.html"><!-- content --><p>Fuleco, la mascota del Mundial</p><p>Brasil 2014.</p></a>
      </div>
     </div>
     <a class="nonblock nontext museBGSize clip_frame grpelem" id="u679" href="fuleco.html"><!-- image --></a>
    </div>
    <div class="grpelem" id="pu677"><!-- inclusion -->
     <a class="nonblock nontext" id="u677" href="blackjack.html"><!-- simple frame --></a>
     <div class="clearfix" id="pu678-4"><!-- group -->
      <a class="nonblock nontext clearfix grpelem" id="u678-4" href="blackjack.html"><!-- content --><p>Introducción al BlackJack.</p></a>
     </div>
    </div>
    <div class="grpelem" id="pu685"><!-- inclusion -->
     <a class="nonblock nontext" id="u685" href="robo.html"><!-- simple frame --></a>
     <div class="clearfix" id="pu686-6"><!-- group -->
      <a class="nonblock nontext clearfix grpelem" id="u686-6" href="robo.html"><!-- content --><p>Robaron 688 entradas para</p><p>el Mundial</p></a>
     </div>
    </div>
    <a class="nonblock nontext museBGSize clip_frame grpelem" id="u675" href="blackjack.html"><!-- image --></a>
    <a class="nonblock nontext clip_frame grpelem" id="u683" href="robo.html"><!-- image --></a>
    <a class="anchor_item grpelem" id="untitled"></a>
   </div>
   <div class="verticalspacer"></div>
  </div>
  <div class="preload_images">
   <img class="preload" src="<%:Url.Content("~/medios/") %>images/001.jpg" alt=""/>
   <img class="preload" src="<%:Url.Content("~/medios/") %>images/002.jpg" alt=""/>
   <img class="preload" src="<%:Url.Content("~/medios/") %>images/003.jpg" alt=""/>
   <img class="preload" src="<%:Url.Content("~/medios/") %>images/004.jpg" alt=""/>
   <img class="preload" src="<%:Url.Content("~/medios/") %>images/005.jpg" alt=""/>
   <img class="preload" src="<%:Url.Content("~/medios/") %>images/006.jpg" alt=""/>
   <!--[if lt IE 9]>
   <img class="preload" src="images/u5652-r-grad.png" alt=""/>
   <img class="preload" src="images/u5652-grad.png" alt=""/>
   <![endif]-->
  </div>
  <!-- JS includes -->
 
  <script src="<%:Url.Content("~/medios/") %>scripts/museutils.js?3865766194" type="text/javascript"></script>
  <script src="<%:Url.Content("~/medios/") %>scripts/jquery.musepolyfill.bgsize.js?291134478" type="text/javascript"></script>
  <script src="<%:Url.Content("~/medios/") %>scripts/webpro.js?3903299128" type="text/javascript"></script>
  <script src="<%:Url.Content("~/medios/") %>scripts/musewpslideshow.js?138381373" type="text/javascript"></script>
  <script src="<%:Url.Content("~/medios/") %>scripts/jquery.museoverlay.js?4250894771" type="text/javascript"></script>
  <script src="<%:Url.Content("~/medios/") %>scripts/touchswipe.js?4156838003" type="text/javascript"></script>
  <script src="<%:Url.Content("~/medios/") %>scripts/jquery.watch.js?4068933136" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
      $(document).ready(function () {
          try {
              Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
              Muse.Utils.prepHyperlinks(true);/* body */
              Muse.Utils.initWidget('#pamphletu5640', function (elem) { new WebPro.Widget.ContentSlideShow(elem, { contentLayout_runtime: 'stack', event: 'click', deactivationEvent: '', autoPlay: true, displayInterval: 4000, transitionStyle: 'horizontal', transitionDuration: 500, hideAllContentsFirst: false, shuffle: false, enableSwipe: true }); });/* #pamphletu5640 */
              Muse.Utils.fullPage('#page');/* 100% height page */
              Muse.Utils.showWidgetsWhenReady();/* body */
              Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
          } catch (e) { Muse.Assert.fail('Error calling selector function:' + e); }
      });
</script>
   </body>
</html>