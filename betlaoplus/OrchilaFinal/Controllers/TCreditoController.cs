﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrchilaFinal.Controllers
{
    public class TCreditoController : Controller
    {
        //
        // GET: /TCredito/
      betdevEntities db = new  betdevEntities();
        public ActionResult Index(string id)
        {
            var tarjeta=new TCreditos();
            tarjeta.Fecha=DateTime.Now ;
            tarjeta.Usuario=Session["usuario"].ToString();
            tarjeta.Monto=decimal.Parse(id)/100;
            db.TCreditos.Add (tarjeta);
            db.SaveChanges();
            ViewBag.monto = id+"00";
            ViewBag.orderid=tarjeta.Id.ToString();
            ViewBag.Digest = digest(id+"00", ViewBag.orderid);
            return View();
        }

        public string digest(string monto,string orderid)
        {
            System.Type objType = System.Type.GetTypeFromProgID("EMSecAsp.SecAsp");
            dynamic comObject = System.Activator.CreateInstance(objType);
            var dig = comObject.GenDigest(monto, orderid, "77130748", "0", "4001", "937");
            return dig;
        }
    }
}
