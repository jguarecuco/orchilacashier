﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrchilaFinal.Models;
using System.Net;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Dynamic;
using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace OrchilaFinal.Controllers
{
    public class ProcesosController : Controller
    {
        //
        // GET: /Procesos/
        betdevEntities db = new betdevEntities();
        public ActionResult Index()
        {
            return View();
        }
        public string transferencia() {
            string usuario = Session["usuario"].ToString();
            var dux = db.dux.Single(f => f.username == usuario);
            var usuarios = db.bo_form_register.Single(f => f.login == usuario);
            var origen=long.Parse(Request.Form["origen"].ToString());
            var destino = long.Parse(Request.Form["destino"].ToString());
            var monto = Request.Form["monto"];
            var kioscode = dux.kioscode;
            var kioscodeadmin = dux.kioskadmincode;
            var caja = dux.TBETS;
          //  if (origen == 1 && destino == 2) {
                string variable = "txtorigen="+origen.ToString()+"&txtdestino="+destino.ToString()+"&txtmonto="+monto+"&usuario="+usuario+"&password="+usuarios.password;
                var data=(string)GetResponseString("http://www.betlaorchila.com:1852/caja/transferencia1/" + DateTime.Now.Ticks.ToString(), variable);
                iresult1 v = Newtonsoft.Json.JsonConvert.DeserializeObject<iresult1>(data);
                return Newtonsoft.Json.JsonConvert.SerializeObject(v);
            //}
           // return "";
        }

        public string cajas(long id) {
            List<banco_internos> cajas = new List<banco_internos>() { };
            if (id == 0) {
                var caja = new banco_internos();
                caja.banco = "Caja Principal";
                caja.id = 1;
                cajas.Add(caja);
             //   var caja1 = new banco_internos();
               // caja1.banco = "Bonos";
                //caja1.id = 2;
                //cajas.Add(caja1);
                var caja2 = new banco_internos();
                caja2.banco = "Casino";
                caja2.id = 2;
                cajas.Add(caja2);
            }
            if (id == 1)
            {
 
                //var caja1 = new banco_internos();
                //caja1.banco = "Bonos";
                //caja1.id = 2;
                //cajas.Add(caja1);
                var caja2 = new banco_internos();
                caja2.banco = "Casino";
                caja2.id = 2;
                cajas.Add(caja2);
            }
            if (id == 2)
            {
                var caja = new banco_internos();
                caja.banco = "Caja Principal";
                caja.id = 1;
                cajas.Add(caja);
 
                var caja2 = new banco_internos();
                caja2.banco = "Casino";
                caja2.id = 2;
                cajas.Add(caja2);
            }
            if (id == 3)
            {
                var caja = new banco_internos();
                caja.banco = "Caja Principal";
                caja.id = 1;
                cajas.Add(caja);
              //  var caja1 = new banco_internos();
               // caja1.banco = "Bonos";
               // caja1.id = 2;
               // cajas.Add(caja1);
 
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(cajas); ;
        }
        String GetResponseString(string url, string poststring)
        {
            HttpWebRequest httpRequest =
            (HttpWebRequest)WebRequest.Create(url);

            httpRequest.Method = "POST";
            httpRequest.ContentType = "application/x-www-form-urlencoded";

            byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
            httpRequest.ContentLength = bytedata.Length;

            Stream requestStream = httpRequest.GetRequestStream();
            requestStream.Write(bytedata, 0, bytedata.Length);
            requestStream.Close();

            HttpWebResponse httpWebResponse =
            (HttpWebResponse)httpRequest.GetResponse();
            Stream responseStream = httpWebResponse.GetResponseStream();
            StringBuilder sb = new StringBuilder();
            using (StreamReader reader =
            new StreamReader(responseStream, System.Text.Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    sb.Append(line);
                }
            }
            return sb.ToString();
        }
        public string deposito()
        {
            string usuario = Session["usuario"].ToString();
            var dux = db.dux.Single(f => f.username == usuario);
            var usuarios = db.bo_form_register.Single(f => f.login == usuario);
            var banco=Request.Form["banco"].ToString();
            var numero = Request.Form["numero"].ToString();
            var monto = Request.Form["monto"];
            var fecha = Request.Form["fecha"];
            var depositos = new depositos();
            depositos.banco = banco;
            depositos.fecha_deposito = DateTime.Parse(fecha.ToString());
            depositos.fecha = DateTime.Now;
            depositos.monto = decimal.Parse(monto.ToString());
            depositos.transaccion = numero;
            depositos.usuario = usuario;
            depositos.Estado = 0;
            depositos.operador = 0;
            db.depositos.Add(depositos);
            db.SaveChanges();
            var transaccion = new transacciones();
            transaccion.banco = banco;
            transaccion.fecha = depositos.fecha ;
            transaccion.id_operacion = depositos.id;
            transaccion.monto = depositos.monto ;
            transaccion.tipo = 2;
            transaccion.usuario = usuario;
            transaccion.desde = "BANCO";
            transaccion.hasta = "CAJA PRINCIPAL";
            db.transacciones.Add(transaccion);
            db.SaveChanges();
            var resultado = new iresult();
            resultado.sucess = "1";
            resultado.msg = "";
            return Newtonsoft.Json.JsonConvert.SerializeObject(resultado);
        }
        public string Saldos()
        {
            string usuario = Session["usuario"].ToString();
            var dux = db.dux.Single(f => f.username == usuario);
            var usuarios = db.bo_form_register.Single(f => f.login == usuario);
            var iproceso = new iSaldos();
            iproceso.Caja = dux.TBETS.Value;
            iproceso.id = dux.id;
            iproceso.Sportsbook = 0;
            iproceso.Casino = Casino();
            iproceso.Total = iproceso.Caja + iproceso.Casino;
            iproceso.Nombre = usuarios.name;
            iproceso.Apellido = usuarios.apellido;
            iproceso.Email = usuarios.email;
            iproceso.Cedula = usuarios.ci;
            iproceso.Usuario = usuarios.login;
            return JsonConvert.SerializeObject(iproceso);
        }

        public decimal Casino()
        {
           // var url = "https://cashier-cur.techonlinecorp.com/get_playerinfo.php?username=" + username + "&casino=inandplay&password=" + password + "&secretkey=ay3vb7iu5ty&responsetype=xml&excludedetails=1";
            var url = "https://cashier-cur.techonlinecorp.com/get_playerinfo.php?username=" + Session["usuario"].ToString() + "&casino=inandplay&password=" + Session["password"].ToString() + "&secretkey=ay3vb7iu5ty&responsetype=xml&excludedetails=1";
           
            // var url = "https://cashier-cur.techonlinecorp.com/get_playerinfo.php?username=V4015&casino=inandplay&password=11284048&secretkey=ay3vb7iu5ty&responsetype=json&excludedetails=1";
            //var url = "http://betlaorchila.com:8885/procesos/casino";
            var web = new WebClient();

           var cadena =  web.DownloadString(url);
           var xDoc = XDocument.Parse(cadena);

           dynamic root = new ExpandoObject();

           XmlToDynamics.Parse(root, xDoc.Elements().First());

           return Decimal.Parse(root.playerinfo.balance);
        }

      public  string bancos_depositos() {
          var bancos = db.banco_internos.ToList();
          return Newtonsoft.Json.JsonConvert.SerializeObject(bancos);
         // return "{'bancos':[{'cod':'1','banco':'Banesco'},{'cod':'2','banco':'Occidental de descuento(B.O.D)'},{'cod':'3','banco':'Provincial'},{'cod':'4','banco':'Venezuela'},{'cod':'5','banco':'Sofitasa'},{'cod':'6','banco':'Bancaribe'},{'cod':'7','banco':'Canjea tu bono'}]}";
        }
      public string bancos() {
          var bancos = db.bancos.ToList();
          return Newtonsoft.Json.JsonConvert.SerializeObject(bancos);
      }
      public string tipo() {
          var bancos = new List<bancos>(){};
          bancos.Add(new bancos());
          bancos[0].id = 1;
          bancos[0].nombre  = "Ahorro";
          bancos.Add(new bancos());
          bancos[1].id = 2;
          bancos[1].nombre = "Corriente";
          return Newtonsoft.Json.JsonConvert.SerializeObject(bancos);

      }
    }
   

}
