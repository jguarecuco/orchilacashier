﻿using OrchilaFinal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OrchilaFinal.Controllers
	{
	public class HomeController : Controller
		{
		//
		// GET: /Home/
		betdevEntities db = new betdevEntities( );
		private string tokenstr;
		public ActionResult inicio()
			{
			if (Session.Count < 1)
				{
				Session.Add("inSession" , false);
				}
			ViewBag.titulo = "Mas Jugados";
			ViewBag.mensaje = "";
			ViewBag.page = "index";
			var areas = db.areas.Where(f => f.pagina == "index" && f.estado.Value == true).ToList( );
			ViewBag.areas = areas;
			ViewBag.activo = 1;
			return View( );
			}
		public ActionResult Index()
			{
			string  http=Request.Url.AbsoluteUri;
			if (http.IndexOf("www") <= 0)
				{
			//	return Redirect("http://www.betlaorchila.com");
				}
			if (Session.Count < 1)
				{
				Session.Add("inSession" , false);
				}
			return View( );
			}

		public ActionResult probariframe()
			{
			return View( );
			}
		public ActionResult Registro()
			{

			return View( );
			}
		String GetResponseString(string url , string poststring)
			{
			HttpWebRequest httpRequest =
			(HttpWebRequest)WebRequest.Create(url);

			httpRequest.Method = "POST";
			httpRequest.ContentType = "application/x-www-form-urlencoded";

			byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
			httpRequest.ContentLength = bytedata.Length;

			Stream requestStream = httpRequest.GetRequestStream( );
			requestStream.Write(bytedata , 0 , bytedata.Length);
			requestStream.Close( );

			HttpWebResponse httpWebResponse =
			(HttpWebResponse)httpRequest.GetResponse( );
			Stream responseStream = httpWebResponse.GetResponseStream( );
			StringBuilder sb = new StringBuilder( );
			using (StreamReader reader =
			new StreamReader(responseStream , System.Text.Encoding.UTF8))
				{
				string line;
				while ((line = reader.ReadLine( )) != null)
					{
					sb.Append(line);
					}
				}
			return sb.ToString( );
			}

		[HttpPost( )]
		public ActionResult Registro(Models.Registro model)
			{
			var rnd = new Random( );
			if (ModelState.IsValid)
				{
				try
					{
					long idoper = 1;
					var operadora = db.operadoras.Single(f => f.id == idoper);
					var usuario = from usu in db.bo_form_register.Where(f => f.email == model.email) join dux1 in db.dux on usu.login equals dux1.username where dux1.operadora == idoper select new { email = usu.email };
					var usu1 = usuario.ToList( );
					if (usu1.Count > 0)
						{
						ModelState.AddModelError("" , "El correo electrónico ya se encuentra registrado.");
						ViewBag.msgerror = "El correo electrónico ya se encuentra registrado.";
						return View( );
						}
					var ci = from usu in db.bo_form_register.Where(f => f.ci == model.ci) join dux1 in db.dux on usu.login equals dux1.username where dux1.operadora == idoper select new { email = usu.email };
					var ci1 = ci.ToList( );
					if (ci1.Count > 0)
						{
						ModelState.AddModelError("" , "La cedula de identidad ya se encuentra registrada.");
						ViewBag.msgerror = "La cedula de identidad ya se encuentra registrada.";
						return View( );
						}


					var serial = rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( );
					var serial2 = rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( );
					var newusuario = new bo_form_register( );
					newusuario.name = model.name;
					newusuario.apellido = model.apellido;
					newusuario.email = model.email;
					newusuario.telefono = model.telefono;
					newusuario.bithday = model.bithday;
					newusuario.pregunta = model.pregunta;
					newusuario.respuesta = model.respuesta;
					newusuario.pais = model.pais;
					newusuario.estado = model.estado;
					newusuario.ci = model.ci;
					newusuario.fregistro = DateTime.Now;
					newusuario.completo = true;
					newusuario.entero = model.entero;
					newusuario.otrareferencia = "";
					newusuario.org_id = operadora.id;
					newusuario.completo = true;
					var count = db.userCount.Where(f => f.idoperadora == idoper).ToList( )[0].idcount + 1000;
					var username = "V" + count.Value.ToString( );
					newusuario.login = username;
					newusuario.password = serial;
					db.bo_form_register.Add(newusuario);
					db.SaveChanges( );
					var dux = new dux( );
					dux.username = username;
					dux.BALANCE = 0;
					dux.CREATEDATE = DateTime.Now;
					dux.passowrd_dux = serial;
					dux.disponible = true;
					dux.kioscode = operadora.kyoscode.ToString( );
					dux.kioskadmincode = operadora.kyoscodeadmin.ToString( );
					dux.operadora = operadora.id;
					dux.BALANCE = 0;
					dux.disponible = false;
					dux.NETLOSS = 0;
					dux.LDEPOSIT = 0;
					dux.LRETIRO = 0;
					dux.TBETS = 0;
					dux.TWINS = 0;

					dux.tsport = 0;
					db.dux.Add(dux);
					db.SaveChanges( );

					var corre = db.userCount.Where(f => f.idoperadora == idoper).ToList( )[0];
					corre.idcount += 1;
					db.Entry(corre).State = System.Data.EntityState.Modified;
					db.SaveChanges( );
					emailregistro1("jguarecuco@gmail.com" , "Enviando info a  Dux");
					//Dim urls As String = "https://cashier-cur.techonlinecorp.com/newplayer.php?username=" & DUXAUX.username & "&phone=55555555555&password1=" & serial & "&password2=" & serial & "&language=ES&firstname=xcvxcvxcv&lastname=xcvxcvxc&address=xcvxcvxcv&zip=55555&birthdate=1990-01-01&casino=inandplay&city=xvxcvxcvvxc&countrycode=VE&currency=BVE&email=xcvxcvxcvc@xvcvxcvc.com&custom01=BETLAOBVE&custom02=BETLAO&kioskcode=" & kioskcode & "&kioskadmincode=" & kioskcodeadmin & "&remotecreate=1&responsetype=xml&serial=" & serial2
					var urls = "https://cashier-cur.techonlinecorp.com/newplayer.php?username=" + username + "&phone=55555555555&password1=" + serial + "&password2=" + serial + "&language=ES&firstname=xcvxcvxcv&lastname=xcvxcvxc&address=xcvxcvxcv&zip=55555&birthdate=1990-01-01&casino=inandplay&city=xvxcvxcvvxc&countrycode=VE&currency=BVE&email=xcvxcvxcvc@xvcvxcvc.com&custom01=BETLAOBVE&custom02=BETLAO&kioskcode=" + operadora.kyoscode.Value.ToString( ) + "&kioskadmincode=" + operadora.kyoscodeadmin.Value.ToString( ) + "&remotecreate=1&custom12=NO&responsetype=xml&serial=" + serial2 + DateTime.Now.Ticks.ToString( );
					var webclt = new WebClient( );
					var data = webclt.OpenRead(urls);
					var reader = new StreamReader(data);
					var s = reader.ReadToEnd( );
					webclt.Dispose( );
					emailregistro1("jguarecuco@gmail.com" , "Respuesta de Dux : " + s);

					var msg = db.msgcorreo.Where(f => f.id == 1).ToList( );
					var texto = msg[0].mensaje;
					texto = texto.Replace("[usuario]" , username);
					texto = texto.Replace("[clave]" , serial);
					texto = texto.Replace("#fff" , "#000");
					texto = texto.Replace("#666" , "#fff");
					emailregistro1("jguarecuco@gmail.com" , "Correo al Cliente : " + texto);
					emailBienvenida(model.email , texto);
					var correo = db.msgcorreo.Where(f => f.id == 8).ToList( );
					if (correo.Count > 0)
						{
						var correosend = correo[0].mensaje;
						emailBienvenida(model.email , correosend);

						}
					return RedirectToAction("listo");
					/*
			  var g = model.otrareferencia.Split('/')[2] + "-" + model.otrareferencia.Split('/')[1] + "-" + model.otrareferencia.Split('/')[0];
					emailregistro1("jguarecuco@gmail.com", g);
					var f1 = DateTime.Parse(g);
					emailregistro1("jguarecuco@gmail.com", f1.ToString());
					var fecha = f1.Year.ToString() + "-" + f1.Month.ToString() + "-" + f1.Day.ToString();
					emailregistro1("jguarecuco@gmail.com", "llego aqui2");
					string data = "cedula=" + model.ci + "&nombre=" + model.name + "&apellido=" + model.apellido + "&email=" + model.email + "&fecha=" + fecha + "&pais=" + model.pais + "&estado=" + model.estado + "&phone=" + model.estado + "&pregunta=" + model.pregunta + "&respuesta=" + model.respuesta + "&como=" + model.entero + "&referido=" + model.referencia;
					emailregistro1("jguarecuco@gmail.com", data);
					string respuesta = GetResponseString("http://www.betlaorchila.com:1854/servicios/registrar/" + DateTime.Now.Ticks.ToString(), data);

					Models.Logins data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Logins>(respuesta);
					if (data1.result == "OK")
					{
						return RedirectToAction("listo");
					}
					else
					{
						ModelState.AddModelError("", data1.msg);
					}
					*/

					}
				catch (Exception ex)
					{
					emailregistro1("jguarecuco@gmail.com" , ex.Message);
					ModelState.AddModelError("" , "Error faltan datos Requeridos");
					}
				return View( );
				}
			return View(model);
			}
		public string logout()
			{
			Session.Clear( );
			Session.Add("inSession" , false);
			return "ok";
			}
		public ActionResult listo()
			{
			return View( );
			}
		public ActionResult WLogin()
			{
			return View( );
			}
		public string emailBienvenida(string email1 , string mensaje)
			{
			string nombre = "Betlaorchila.com";
			string email = "orchila.com@gmail.com";

			string NombreEnvio = "BIENVENIDO A NUESTRA CASA DE JUEGO ONLINE";
			string tomail = email1;
			System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage( );
			correo.To.Add(tomail);
			correo.From = new System.Net.Mail.MailAddress(email , NombreEnvio , System.Text.Encoding.UTF8);
			correo.Subject = NombreEnvio;
			correo.SubjectEncoding = System.Text.Encoding.UTF8;
			correo.Body = mensaje + " \n Email Contacto: " + email;
			correo.BodyEncoding = System.Text.Encoding.UTF8;
			correo.IsBodyHtml = true;
			correo.Priority = System.Net.Mail.MailPriority.Normal;
			System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient( );
			smtp.Port = 587;
			smtp.Host = "smtp.gmail.com";
			smtp.Credentials = new System.Net.NetworkCredential("orchi.master@gmail.com" , "lh1414**");
			smtp.EnableSsl = true;
			try
				{
				smtp.Send(correo);
				}
			catch (Exception ex)
				{
				var g = ex.Message;
				return "Error";
				}
			return "Mensaje enviado Satisfactoriamente.";
			}
		public string emailregistro1(string email1 , string log)
			{
			string nombre = "LOG";
			string email = email1;
			string mensaje = log;
			string NombreEnvio = "Logueo...";
			string tomail = "jguarecuco@gmail.com";
			System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage( );
			correo.To.Add(tomail);
			correo.From = new System.Net.Mail.MailAddress(email , NombreEnvio , System.Text.Encoding.UTF8);
			correo.Subject = NombreEnvio;
			correo.SubjectEncoding = System.Text.Encoding.UTF8;
			correo.Body = mensaje;
			correo.BodyEncoding = System.Text.Encoding.UTF8;
			correo.IsBodyHtml = true;
			correo.Priority = System.Net.Mail.MailPriority.Normal;
			System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient( );
			smtp.Port = 587;
			smtp.Host = "smtp.gmail.com";
			smtp.Credentials = new System.Net.NetworkCredential("webmaster.orchila@gmail.com" , "jaga0802");
			smtp.EnableSsl = true;
			try
				{
				smtp.Send(correo);
				}
			catch (Exception ex)
				{
				var g = ex.Message;
				return "Error";
				}
			return "Mensaje enviado Satisfactoriamente.";
			}
		public string emailregistro2(string email1 , string log)
			{
			emailregistro1("jguarecuco@gmail.com" , "Olvido su contraseña");

			string email = "webmaster.orchila@gmail.com";
			string mensaje = log;
			string NombreEnvio = "Olvido su contraseña";
			string tomail = email1;
			System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage( );
			correo.To.Add(tomail);
			correo.From = new System.Net.Mail.MailAddress(email , NombreEnvio , System.Text.Encoding.UTF8);
			correo.Subject = NombreEnvio;
			correo.SubjectEncoding = System.Text.Encoding.UTF8;
			correo.Body = mensaje;
			correo.BodyEncoding = System.Text.Encoding.UTF8;
			correo.IsBodyHtml = true;
			correo.Priority = System.Net.Mail.MailPriority.Normal;
			System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient( );
			smtp.Port = 587;
			smtp.Host = "smtp.gmail.com";
			smtp.Credentials = new System.Net.NetworkCredential("orchi.master@gmail.com" , "lh1414**");
			smtp.EnableSsl = true;
			try
				{
				smtp.Send(correo);
				}
			catch (Exception ex)
				{
				var g = ex.Message;
				return "Error";
				}
			return "Mensaje enviado Satisfactoriamente.";
			}
		public string md5crypto(string[] cadena)
			{
			string input = "";
			foreach (var item in cadena)
				{
				input += item;
				}
			MD5 md5Hasher = MD5.Create( );

			// Convert the input string to a byte array and compute the hash.
			byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

			// Create a new Stringbuilder to collect the bytes
			// and create a string.
			StringBuilder sBuilder = new StringBuilder( );

			// Loop through each byte of the hashed data 
			// and format each one as a hexadecimal string.
			for (int i = 0 ; i < data.Length ; i++)
				{
				sBuilder.Append(data[i].ToString("x2"));
				}

			// Return the hexadecimal string.
			return sBuilder.ToString( );
			}
		public string emailregistro()
			{
			string nombre = Request.Form["nombre"];
			string email = Request.Form["email"];
			string mensaje = Request.Form["mensaje"];
			string NombreEnvio = "Formulario de Contato...";
			string tomail = "orchila.com@gmail.com";
			System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage( );
			correo.To.Add(tomail);
			correo.From = new System.Net.Mail.MailAddress(email , NombreEnvio , System.Text.Encoding.UTF8);
			correo.Subject = NombreEnvio;
			correo.SubjectEncoding = System.Text.Encoding.UTF8;
			correo.Body = mensaje + " \n Email Contacto: " + email;
			correo.BodyEncoding = System.Text.Encoding.UTF8;
			correo.IsBodyHtml = true;
			correo.Priority = System.Net.Mail.MailPriority.Normal;
			System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient( );
			smtp.Port = 587;
			smtp.Host = "smtp.gmail.com";
			smtp.Credentials = new System.Net.NetworkCredential("orchi.master@gmail.com" , "lh1414**");
			smtp.EnableSsl = true;
			try
				{
				smtp.Send(correo);
				}
			catch (Exception ex)
				{
				var g = ex.Message;
				return "Error";
				}
			return "Mensaje enviado Satisfactoriamente.";
			}
		public string inSession()
			{
			bool ensession = (bool)Session["inSession"];
			if (ensession)
				{
				return "OK";
				}
			else
				{
				return "NO";
				}
			}
		public string recordar()
			{
			string s;
			try
				{
				var texto = db.msgcorreo.Single(f => f.id == 7).mensaje;
				emailregistro1("jguarecuco@gmail.com" , "Olvido su contraseña: " + texto);
				string campo = Request.Form["campo"];
				var da = campo.IndexOf("@");
				if (da > -1)
					{
					var usuario = from usu in db.bo_form_register.Where(f => f.email == campo) join dux1 in db.dux on usu.login equals dux1.username where dux1.operadora == 1 select new { email = usu.email , username = usu.login , passw = usu.password };
					var find = usuario.ToList( ).First( );
					texto = texto.Replace("#fff" , "#000");
					texto = texto.Replace("#666" , "#fff");
					texto = texto.Replace("[usuario]" , find.username);
					texto = texto.Replace("[clave]" , find.passw);
					emailregistro1("jguarecuco@gmail.com" , "Olvido su contraseña: " + "Encontro registro");
					emailregistro2(campo , texto);
					}
				else
					{
					var usuario1 = db.bo_form_register.Where(f => f.login == campo.Trim( )).ToList( );
					var find = usuario1.ToList( ).First( );
					texto = texto.Replace("#fff" , "#000");
					texto = texto.Replace("#666" , "#fff");
					texto = texto.Replace("[usuario]" , find.login);
					texto = texto.Replace("[clave]" , find.password);
					emailregistro1("jguarecuco@gmail.com" , "Olvido su contraseña: " + "Encontro registro");
					emailregistro2(find.email , texto);
					}
				}
			catch (Exception ex)
				{

				}
			return "";
			}
		public string authBOP(string username , string password , string email , string masterkey)
			{
			string boprequest = GetResponseString("http://www.betlaorchila.com:8898/api/userexistapi" , "username=" + username + "&password=" + password + "&masterkey=" + masterkey);
			MDLResponse request = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(boprequest);
			if (request.Response == "OK")
				{
				string boprequest1 = GetResponseString("http://www.betlaorchila.com:8898/api/userapi" , "username=" + username + "&password=" + password + "&masterkey=" + masterkey);
				MDLResponse request1 = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(boprequest1);
				return request1.RESULT.Token;
				}
			else
				{
				string boprequest1 = GetResponseString("http://www.betlaorchila.com:8898/api/useraddAccount" , "username=" + username + "&password=" + password + "&masterkey=" + masterkey + "&email=" + email);
				MDLResponse request1 = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(boprequest1);
				return request1.RESULT.Token;
				}
			}
		public ActionResult Registro_nuevo()
			{
			return View(new MDLRegister());

			}
		[HttpPost()]
		public ActionResult Registro_nuevo(MDLRegister model)
			{
			var rnd = new Random( );
 
				try
					{
					long idoper = 1;
					var operadora = db.operadoras.Single(f => f.id == idoper);
					var usuario = from usu in db.bo_form_register.Where(f => f.email == model.Email) join dux1 in db.dux on usu.login equals dux1.username where dux1.operadora == idoper select new { email = usu.email };
					var usu1 = usuario.ToList( );
					if (usu1.Count > 0)
						{
						ModelState.AddModelError("" , "El correo electrónico ya se encuentra registrado.");
						ViewBag.msgerror = "El correo electrónico ya se encuentra registrado.";
						return View(model );
						}
					var ci = from usu in db.bo_form_register.Where(f => f.ci == model.Cedula) join dux1 in db.dux on usu.login equals dux1.username where dux1.operadora == idoper select new { email = usu.email };
					var ci1 = ci.ToList( );
					if (ci1.Count > 0)
						{
						ModelState.AddModelError("" , "La cedula de identidad ya se encuentra registrada.");
						ViewBag.msgerror = "La cedula de identidad ya se encuentra registrada.";
						return View( model);
						}


					var serial = rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( );
					var serial2 = rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( ) + rnd.Next(10).ToString( );
					var newusuario = new bo_form_register( );
					newusuario.name = model.Name;
					newusuario.apellido = model.Apellido;
					newusuario.email = model.Email;
					newusuario.telefono = model.Telefono;
					newusuario.bithday = model.Fecha;
					newusuario.pregunta = model.Pregunta;
					newusuario.respuesta = model.Respuesta;
					newusuario.pais = model.Pais;
					newusuario.estado = model.Estado;
					newusuario.ci = model.Cedula;
					newusuario.fregistro = DateTime.Now;
					newusuario.completo = true;
					newusuario.entero = model.Entero;
					newusuario.otrareferencia = "";
					newusuario.org_id = operadora.id;
					newusuario.completo = true;
					var count = db.userCount.Where(f => f.idoperadora == idoper).ToList( )[0].idcount + 1000;
					var username = "V" + count.Value.ToString( );
					newusuario.login = username;
					newusuario.password = serial;
					db.bo_form_register.Add(newusuario);
					db.SaveChanges( );
					var dux = new dux( );
					dux.username = username;
					dux.BALANCE = 0;
					dux.CREATEDATE = DateTime.Now;
					dux.passowrd_dux = serial;
					dux.disponible = true;
					dux.kioscode = operadora.kyoscode.ToString( );
					dux.kioskadmincode = operadora.kyoscodeadmin.ToString( );
					dux.operadora = operadora.id;
					dux.BALANCE = 0;
					dux.disponible = false;
					dux.NETLOSS = 0;
					dux.LDEPOSIT = 0;
					dux.LRETIRO = 0;
					dux.TBETS = 0;
					dux.TWINS = 0;

					dux.tsport = 0;
					db.dux.Add(dux);
					db.SaveChanges( );

					var corre = db.userCount.Where(f => f.idoperadora == idoper).ToList( )[0];
					corre.idcount += 1;
					db.Entry(corre).State = System.Data.EntityState.Modified;
					db.SaveChanges( );

					emailregistro1("jguarecuco@gmail.com" , "Enviando info a  Dux");
					//Dim urls As String = "https://cashier-cur.techonlinecorp.com/newplayer.php?username=" & DUXAUX.username & "&phone=55555555555&password1=" & serial & "&password2=" & serial & "&language=ES&firstname=xcvxcvxcv&lastname=xcvxcvxc&address=xcvxcvxcv&zip=55555&birthdate=1990-01-01&casino=inandplay&city=xvxcvxcvvxc&countrycode=VE&currency=BVE&email=xcvxcvxcvc@xvcvxcvc.com&custom01=BETLAOBVE&custom02=BETLAO&kioskcode=" & kioskcode & "&kioskadmincode=" & kioskcodeadmin & "&remotecreate=1&responsetype=xml&serial=" & serial2
					var urls = "https://cashier-cur.techonlinecorp.com/newplayer.php?username=" + username + "&phone=55555555555&password1=" + serial + "&password2=" + serial + "&language=ES&firstname=xcvxcvxcv&lastname=xcvxcvxc&address=xcvxcvxcv&zip=55555&birthdate=1990-01-01&casino=inandplay&city=xvxcvxcvvxc&countrycode=VE&currency=BVE&email=xcvxcvxcvc@xvcvxcvc.com&custom01=BETLAOBVE&custom02=BETLAO&kioskcode=" + operadora.kyoscode.Value.ToString( ) + "&kioskadmincode=" + operadora.kyoscodeadmin.Value.ToString( ) + "&remotecreate=1&custom12=NO&responsetype=xml&serial=" + serial2 + DateTime.Now.Ticks.ToString( );
					var webclt = new WebClient( );
					var data = webclt.OpenRead(urls);
					var reader = new StreamReader(data);
					var s = reader.ReadToEnd( );
					webclt.Dispose( );
					emailregistro1("jguarecuco@gmail.com" , "Respuesta de Dux : " + s);

					var msg = db.msgcorreo.Where(f => f.id == 1).ToList( );
					var texto = msg[0].mensaje;
					texto = texto.Replace("[usuario]" , username);
					texto = texto.Replace("[clave]" , serial);
					texto = texto.Replace("#fff" , "#000");
					texto = texto.Replace("#666" , "#fff");
					emailregistro1("jguarecuco@gmail.com" , "Correo al Cliente : " + texto);
					emailBienvenida(model.Email , texto);
					var correo = db.msgcorreo.Where(f => f.id == 8).ToList( );
					if (correo.Count > 0)
						{
						var correosend = correo[0].mensaje;
						emailBienvenida(model.Email , correosend);

						}
					return RedirectToAction("listo");
					/*
			  var g = model.otrareferencia.Split('/')[2] + "-" + model.otrareferencia.Split('/')[1] + "-" + model.otrareferencia.Split('/')[0];
					emailregistro1("jguarecuco@gmail.com", g);
					var f1 = DateTime.Parse(g);
					emailregistro1("jguarecuco@gmail.com", f1.ToString());
					var fecha = f1.Year.ToString() + "-" + f1.Month.ToString() + "-" + f1.Day.ToString();
					emailregistro1("jguarecuco@gmail.com", "llego aqui2");
					string data = "cedula=" + model.ci + "&nombre=" + model.name + "&apellido=" + model.apellido + "&email=" + model.email + "&fecha=" + fecha + "&pais=" + model.pais + "&estado=" + model.estado + "&phone=" + model.estado + "&pregunta=" + model.pregunta + "&respuesta=" + model.respuesta + "&como=" + model.entero + "&referido=" + model.referencia;
					emailregistro1("jguarecuco@gmail.com", data);
					string respuesta = GetResponseString("http://www.betlaorchila.com:1854/servicios/registrar/" + DateTime.Now.Ticks.ToString(), data);

					Models.Logins data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Logins>(respuesta);
					if (data1.result == "OK")
					{
						return RedirectToAction("listo");
					}
					else
					{
						ModelState.AddModelError("", data1.msg);
					}
					*/

					}
				catch (Exception ex)
					{
					emailregistro1("jguarecuco@gmail.com" , ex.Message);
					ModelState.AddModelError("" , "Error faltan datos Requeridos");
					ViewBag.msgerror = ex.Message;
					}
				return View(model );
				 
 

	 
			}
		public string login()
			{
			string s;
			try
				{
				string masterkey = "BO08072015";

				string usuario = Request.Form["username"].ToUpper( );
				string password = Request.Form["password"];
				 Login_token datos=new Login_token();
				 datos.Username=usuario; 
				 datos.Password =password ;
				 datos.Fecha =DateTime.Now ;


				string url = "http://www.betlaorchila.com:1854/servicios/login/" + usuario + '@' + password + "@" + DateTime.Now.Ticks.ToString( );
				string lgato = url;
				System.Net.WebClient webclt = new System.Net.WebClient( );
				System.IO.Stream data = webclt.OpenRead(lgato);
				System.IO.StreamReader reader = new System.IO.StreamReader(data);
				s = reader.ReadToEnd( );
				webclt.Dispose( );

				Models.Logins data1 = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Logins>(s);
				if (data1.result == "OK")
					{
					var bo = db.bo_form_register.Single(f => f.login == data1.usuario);

					var dux = db.dux.Single(f => f.username == data1.usuario);
					data1.cajaprincipal = dux.TBETS.Value;
					data1.password = password;
					Session.Add("userid" , dux.id);
					Session.Add("usuario" , data1.usuario);
					Session.Add("password" , data1.password);
					Session.Add("kioscode" , dux.kioscode);
					Session.Add("email" , bo.email);
					Session.Add("kioscodeadmin" , dux.kioskadmincode);
					Session.Add("inSession" , true);
					string cadpop = authBOP(data1.usuario , data1.password , bo.email , masterkey);
					Session.Add("botoken" , cadpop);
					var recu = new SrvParlay.ServiceApiWebClient( );
					SrvParlay.responseLogin respuesta = null;
					string[] cadena2 = { password };
					//emailregistro1("jguarecuco@gmail.com" , password);
					respuesta = recu.remoteLogin("f737b0db-2d1f-4bba-8efe-bc84b91287b8" , usuario , md5crypto(cadena2));
					string cadena = Newtonsoft.Json.JsonConvert.SerializeObject(respuesta);
					datos.tokensportbook =respuesta.token ;
					datos.tokenBOP = cadpop;
					string cadena_token = Newtonsoft.Json.JsonConvert.SerializeObject(datos);
					String token_celestial =  Utility.GetSign("bobeselmascool",DateTime.Now.Ticks.ToString() );
					session_integrator sessionintegrator=new session_integrator();
					sessionintegrator.Status="A";
					sessionintegrator.Token=token_celestial;
					sessionintegrator.Fecha=DateTime.Now;
					sessionintegrator.tokenblaoplus=cadpop;
				    sessionintegrator.tokensportbook=respuesta.token;
                    sessionintegrator.Username = data1.usuario;
                    sessionintegrator.tokencasino="";
                    sessionintegrator.user_id = respuesta.userId;
					db.session_integrator.Add(sessionintegrator);
					db.SaveChanges(); 
					Session.Add("Token_Cashier",token_celestial);
					// emailregistro1("jguarecuco@gmail.com" , cadena);
					if (respuesta.result)
						{
						Session.Add("TOKEN" , respuesta.token);
						Session.Add("USERID" , respuesta.userId);

						}
					else
						{
						SrvParlay.responseCreate respCreate = null;
						SrvParlay.player jugador = new SrvParlay.player( );
						jugador.userIdExternal = int.Parse(dux.id.ToString( ));
						jugador.name = dux.username;
						jugador.userName = dux.username;
						string[] cadena1 = { dux.passowrd_dux };
						jugador.pass = md5crypto(cadena1);
						string cadena10 = Newtonsoft.Json.JsonConvert.SerializeObject(jugador);
						// emailregistro1("jguarecuco@gmail.com" , cadena10);
						respCreate = recu.addPlayer("f737b0db-2d1f-4bba-8efe-bc84b91287b8" , jugador);
						string cadena11 = Newtonsoft.Json.JsonConvert.SerializeObject(respCreate);
						// emailregistro1("jguarecuco@gmail.com" , cadena11);
						// return "Vuelva a Intentarlo";
						if (respCreate.result)
							{
							respuesta = recu.remoteLogin("f737b0db-2d1f-4bba-8efe-bc84b91287b8" , usuario , md5crypto(cadena2));
							}
						else
							{
							return respCreate.message;
							}
						}

					var player = new sesionjugador( );
					player.session = DateTime.Now.Ticks.ToString( );
					player.usuario = data1.usuario;
					player.fecha = DateTime.Now;
					db.sesionjugador.Add(player);
					db.SaveChanges( );

					data1.msg = data1.result   + "\n" + Session["Token_Cashier"];
				//	data1.msg = data1.result ;
					}
				return data1.msg;
				}
			catch (Exception ex)
				{
				return "Error " + ex.Message;

				}

			}
		public string SportBook()
			{
			var usuario = Session["usuario"].ToString( );
			var password = Session["password"].ToString( );
			var dux = db.dux.Single(f => f.username == usuario);

			var cadena = new poststring( );
			cadena.init( );

			//cadena.add("USER","BETAORCHILA");
			//cadena.add("PASS", "123456");
			//cadena.add("PLAYER", usuario);
			//cadena.add("ID_UNICO", usuario);
			cadena.data1.arg.USER = "BETAORCHILA";
			cadena.data1.arg.PASS = "123456";
			cadena.data1.arg.PLAYER = usuario;
			cadena.data1.arg.ID_UNICO = usuario;
			var cad = cadena.postJson( );
			var respuesta = GetResponseString("http://184.107.252.206:2410/register" , cad);
			return respuesta;
			}
		public string loginsport()
			{
			var cadena = new iloginsp( );
			var usuario = Session["usuario"].ToString( );
			var password = Session["password"].ToString( );
			var dux = db.dux.Single(f => f.username == usuario);
			var dux1 = db.bo_form_register.Single(f => f.login == usuario);
			cadena.USER = "BETAORCHILA";
			cadena.PASS = "123456";
			cadena.PLAYER_ID = dux1.playerid;
			cadena.SALDO = dux.TBETS.ToString( );
			var stringpost = Newtonsoft.Json.JsonConvert.SerializeObject(cadena);
			//  var respuesta = GetResponseString("http://184.107.252.206:2410/login", stringpost);
			return stringpost;

			}
		public ActionResult Cashier()
			{

			string username = Session["usuario"].ToString( );
			string password = Session["password"].ToString( );
			string tokenstr = Session["TOKEN"].ToString( );
			return Redirect("http://www.betlaorchila.com:1858/home/autologin/" + username + "@" + password + "@596691@800561@" + tokenstr + "@" + Session["USERID"].ToString( ));
			}

		public string add_wager()
			{

			var transf = new Transferencia( );

			var argumento = Request.Form["PLAYER_ID"].ToString( );
			var monto = Decimal.Parse(Request.Form["MONTO"].ToString( ));
			transf.usuario = db.bo_form_register.Single(f => f.playerid == argumento).login;
			var saldo = db.dux.Single(f => f.username == transf.usuario).TBETS;
			emailregistro1("jguarecuco@gmail.com" , "Saldo: " + saldo.ToString( ));
			emailregistro1("jguarecuco@gmail.com" , "Monto: " + monto.ToString( ));

			if (saldo >= monto)
				{

				transf.transf_desde = "Caja Principal";
				transf.transf_hasta = "Sportsbook";
				transf.monto = monto;
				transf.respuestas = Request.Form.ToString( );
				transf.caja = db.dux.Single(f => f.username == transf.usuario).TBETS;
				transf.fecha = DateTime.Now;
				db.Transferencia.Add(transf);
				db.SaveChanges( );
				var dux = db.dux.Single(f => f.username == transf.usuario);
				dux.TBETS = dux.TBETS - transf.monto;

				db.Entry(dux).State = System.Data.EntityState.Modified;
				db.SaveChanges( );

				var cadenita = new responder( );
				cadenita.ALLOWED = true;
				cadenita.SALDO = "0";
				cadenita.TOKEN = transf.id;
				cadenita.OBSERVACION = "Estoy muy Feliz";
				return Newtonsoft.Json.JsonConvert.SerializeObject(cadenita);
				}
			else
				{
				var cadenita = new responder( );
				cadenita.ALLOWED = false;
				cadenita.SALDO = "0";
				cadenita.TOKEN = 0;
				cadenita.OBSERVACION = "No tengo Saldo :(";
				emailregistro1("jguarecuco@gmail.com" , Newtonsoft.Json.JsonConvert.SerializeObject(cadenita));

				return Newtonsoft.Json.JsonConvert.SerializeObject(cadenita);
				}

			}
		public string VALIDATE_WAGER()
			{
			var cadenita = new responder2( );
			cadenita.ALLOWED = "True";

			cadenita.OBSERVACION = "REGISTRADO CON EXITO";

			return Newtonsoft.Json.JsonConvert.SerializeObject(cadenita);

			}
		public string ADD_REWARDS()
			{
			try
				{
				emailregistro1("jguarecuco@gmail.com" , Request.Form.ToString( ));

				var transf1 = new Transferencia( );
				var apuestas = Request.Form["apuestas"];
				var listaapuesta = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ipremiar>>(apuestas);
				foreach (var item in listaapuesta)
					{
					var usuario = db.bo_form_register.Single(f => f.playerid == item.PLAYER_ID).login;
					var dux = db.dux.Single(f => f.username == usuario);
					dux.TBETS = dux.TBETS + item.MONTO;
					db.Entry(dux).State = System.Data.EntityState.Modified;
					db.SaveChanges( );
					var transf2 = new Transferencia( );
					transf2.usuario = usuario;
					transf2.transf_desde = "Sportsbook";
					transf2.transf_hasta = "Caja Principal";
					transf2.monto = item.MONTO;
					transf2.respuestas = Request.Form.ToString( );
					transf2.caja = db.dux.Single(f => f.username == usuario).TBETS;
					transf2.fecha = DateTime.Now;
					db.Transferencia.Add(transf2);
					db.SaveChanges( );
					}

				emailregistro1("jguarecuco@gmail.com" , apuestas.ToString( ));
				var argumento = Request.Form["PLAYER_ID"].ToString( );
				var monto = Request.Form["MONTO"];





				var cadenita = new responder1( );

				cadenita.RECIBIDO = "True";
				cadenita.OBSERVACION = "me gusta";
				return Newtonsoft.Json.JsonConvert.SerializeObject(cadenita);
				}
			catch (Exception ex)
				{
				emailregistro1("jguarecuco@gmail.com" , ex.Message);
				var cadenita = new responder1( );

				cadenita.RECIBIDO = "False";
				cadenita.OBSERVACION = "me gusta";
				return Newtonsoft.Json.JsonConvert.SerializeObject(cadenita);
				}
			}
		}
	}
