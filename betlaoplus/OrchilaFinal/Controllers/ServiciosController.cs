﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OrchilaFinal.Controllers
{
    public class ServiciosController : Controller
    {
        //
        // GET: /Servicios/
		betdevEntities db=new betdevEntities();

        public ActionResult Index()
        {
            return View();
        }
		public string sesionoff()
			{
				Session.Clear();
				return "{'result':'OK','msg':''}" ;
			}
        public ActionResult success()
        {
            return View();
        }
        public ActionResult fail() {
            return View();
        }
        public ActionResult RecivoCredito()
        {
            long orderid = long.Parse(Request.Form["orderid"].ToString());
            decimal monto = decimal.Parse(Request.Form["total"].ToString()) / 100;

            if (Request.Form["Respuesta"].ToString() == "DENIED") {
                var tcred = db.TCreditos.Single(f => f.Id == orderid);
                tcred.Estado = "R";
                tcred.Receive_data = Request.Form.ToString();
                db.Entry(tcred).State = System.Data.EntityState.Modified;
                db.SaveChanges();
                emailregistro1("jguarecuco@gmail.com", Request.Form.ToString());
                return RedirectToAction("fail");
            }
            else
            {
                
                var tcred = db.TCreditos.Single(f => f.Id == orderid);
                var dux = db.dux.Single(f => f.username == tcred.Usuario);
                dux.TBETS = dux.TBETS + monto;
                tcred.Estado = "A";
                tcred.Receive_data = Request.Form.ToString();
                db.Entry(tcred).State = System.Data.EntityState.Modified;
                db.SaveChanges();
                db.Entry(dux).State = System.Data.EntityState.Modified;
                db.SaveChanges();


                emailregistro1("jguarecuco@gmail.com", Request.Form.ToString());

                return RedirectToAction("success");
            }

             
        }
        public string emailregistro1(string email1, string log)
        {
            string nombre = "LOG";
            string email = email1;
            string mensaje = log;
            string NombreEnvio = "Logueo...";
            string tomail = "jguarecuco@gmail.com";
            System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
            correo.To.Add(tomail);
            correo.From = new System.Net.Mail.MailAddress(email, NombreEnvio, System.Text.Encoding.UTF8);
            correo.Subject = NombreEnvio;
            correo.SubjectEncoding = System.Text.Encoding.UTF8;
            correo.Body = mensaje;
            correo.BodyEncoding = System.Text.Encoding.UTF8;
            correo.IsBodyHtml = true;
            correo.Priority = System.Net.Mail.MailPriority.Normal;
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com";
            smtp.Credentials = new System.Net.NetworkCredential("webmaster.orchila@gmail.com", "jaga0802");
            smtp.EnableSsl = true;
            try
            {
                smtp.Send(correo);
            }
            catch (Exception ex)
            {
                var g = ex.Message;
                return "Error";
            }
            return "Mensaje enviado Satisfactoriamente.";
        }
		public string login(string id)
			{
			var host = Request.Url.Host;
			var usuario = id.Split('@')[0].ToUpper( );
			var password = id.Split('@')[1].ToUpper( );
			try { 
				var visita=new  session();
				visita.fecha=DateTime.Now;
				visita.serial=DateTime.Now.Ticks.ToString();
				visita.usuario=usuario;
				try
					{
					visita.ip = Request.UserHostAddress;
					}
				catch (Exception ex)
					{
					  Console.WriteLine(ex.Message);
					}
			  	db.session.Add(visita);
				db.SaveChanges();
				var usuarios = db.dux.Single(f=> f.username == usuario && f.passowrd_dux == password);
				var registro= db.bo_form_register.Single(f=>f.login==usuario);
				var operadora=db.operadoras.Single(f=>f.host==host);
				Session.Add("usuario", usuario);
                Session.Add("password", password);
                Session.Add("kioscode", operadora.kyoscode);
                Session.Add("kioscodeadmin", operadora.kyoscodeadmin);

				return "{'result':'OK','usuario':'" + usuarios.username + "','msg':''}"	;
			}
			catch (Exception ex) { 
				return "{'result':'FAIL','usuario':'" + usuario + "','msg':'Usuario y/o contraseña no coinciden.','host':'" + host + "'}" ;
			}
			}
    }
}
