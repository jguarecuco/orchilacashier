﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Net;

namespace OrchilaFinal
	{
	public class CommonFunction
		{

		public static byte[] Encryptor(string str)
			{
			string EncrptKey = "lh1414**";
			byte[] byKey = { };
			byte[] IV = { 18 , 52 , 86 , 120 , 144 , 171 , 205 , 239 };
			byKey = System.Text.Encoding.UTF8.GetBytes(EncrptKey.Substring(0 , 8));
			DESCryptoServiceProvider des = new DESCryptoServiceProvider( );
			byte[] inputByteArray = Encoding.UTF8.GetBytes(str);
			MemoryStream ms = new MemoryStream( );
			CryptoStream cs = new CryptoStream(ms , des.CreateEncryptor(byKey , IV) , CryptoStreamMode.Write);
			cs.Write(inputByteArray , 0 , inputByteArray.Length);
			cs.FlushFinalBlock( );
			return ms.ToArray( );
			}

		public static string Decryptor(string str)
			{
			str = str.Replace(" " , "+");
			string DecryptKey = "lh1414**";
			byte[] byKey = { };
			byte[] IV = { 18 , 52 , 86 , 120 , 144 , 171 , 205 , 239 };
			byte[] inputByteArray = new byte[str.Length];
			byKey = System.Text.Encoding.UTF8.GetBytes(DecryptKey.Substring(0 , 8));
			DESCryptoServiceProvider des = new DESCryptoServiceProvider( );
			inputByteArray = Convert.FromBase64String(str.Replace(" " , "+"));
			MemoryStream ms = new MemoryStream( );
			CryptoStream cs = new CryptoStream(ms , des.CreateDecryptor(byKey , IV) , CryptoStreamMode.Write);
			cs.Write(inputByteArray , 0 , inputByteArray.Length);
			cs.FlushFinalBlock( );
			System.Text.Encoding encoding = System.Text.Encoding.UTF8;
			return encoding.GetString(ms.ToArray( ));
			}
		}

	public static class HttpRequestMessageExtensions
		{
		private const string HttpContext = "MS_HttpContext";
		private const string RemoteEndpointMessage =
			"System.ServiceModel.Channels.RemoteEndpointMessageProperty";
		private const string OwinContext = "MS_OwinContext";
		public static string GetClientIpAddress(this HttpRequestMessage request)
			{
			if (request.Properties.ContainsKey(HttpContext))
				{
				dynamic ctx = request.Properties[HttpContext];
				if (ctx != null)
					{
					return ctx.Request.UserHostAddress;
					}
				}
			if (request.Properties.ContainsKey(RemoteEndpointMessage))
				{
				dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
				if (remoteEndpoint != null)
					{
					return remoteEndpoint.Address;
					}
				}
			if (request.Properties.ContainsKey(OwinContext))
				{
				dynamic owinContext = request.Properties[OwinContext];
				if (owinContext != null)
					{
					return owinContext.Request.RemoteIpAddress;
					}
				}
			return null;
			}
		}
	public class Utility
		{
		public static string GetSign(string key , string message)
			{
			System.Security.Cryptography.HMAC hmac = System.Security.Cryptography.HMAC.Create("HMACSHA256");
			hmac.Key = System.Text.Encoding.UTF8.GetBytes(key);
			byte[] hash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(message));
			return BitConverter.ToString(hash).Replace("-" , "").ToUpperInvariant( );
			}
		public static string formData(Dictionary<string , string> postdata)
			{
			string stringdata = "";
			foreach (var item in postdata)
				{
				stringdata += item.Key + "=" + item.Value + "&";
				}
 
			return stringdata;
			}

		public static bool isAuth(HttpSessionStateBase session)
			{
			return session.Count == 0 ? false : true;


			}

		public static string HTTPPost(Dictionary<string , string> postdata , string url)
			{
			var postData = formData(postdata);
			// Create a request using a URL that can receive a post. 
			WebRequest request = WebRequest.Create(url);
			// Set the Method property of the request to POST.
			request.Method = "POST";
			// Create POST data and convert it to a byte array.
			// string postData = "This is a test that posts this string to a Web server.";
			byte[] byteArray = Encoding.UTF8.GetBytes(postData.ToString( ));
			// Set the ContentType property of the WebRequest.
			request.ContentType = "application/x-www-form-urlencoded";
			// Set the ContentLength property of the WebRequest.
			request.ContentLength = byteArray.Length;
			// Get the request stream.
			Stream dataStream = request.GetRequestStream( );
			// Write the data to the request stream.
			dataStream.Write(byteArray , 0 , byteArray.Length);
			// Close the Stream object.
			dataStream.Close( );
			// Get the response.
			WebResponse response = request.GetResponse( );
			// Display the status.
			Console.WriteLine(((HttpWebResponse)response).StatusDescription);
			// Get the stream containing content returned by the server.
			dataStream = response.GetResponseStream( );
			// Open the stream using a StreamReader for easy access.
			StreamReader reader = new StreamReader(dataStream);
			// Read the content.
			string responseFromServer = reader.ReadToEnd( );
			// Display the content.
			Console.WriteLine(responseFromServer);
			// Clean up the streams.
			reader.Close( );
			dataStream.Close( );
			response.Close( );
			return responseFromServer;
			}

		}
	}