﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace OrchilaFinal
{
    public class XmlToDynamics
    {
        public static void Parse(dynamic parent, XElement node)
        {
            if (node.HasElements)
            {
                if (node.Elements(node.Elements().First().Name.LocalName).Count() > 1)
                {
                    //list
                    var item = new ExpandoObject();
                    var list = new List<dynamic>();
                    foreach (var element in node.Elements())
                    {
                        Parse(list, element);
                    }

                    AddProperty(item, node.Elements().First().Name.LocalName, list);
                    AddProperty(parent, node.Name.ToString(), item);
                }
                else
                {
                    var item = new ExpandoObject();

                    foreach (var attribute in node.Attributes())
                    {
                        AddProperty(item, attribute.Name.ToString(), attribute.Value.Trim());
                    }

                    //element
                    foreach (var element in node.Elements())
                    {
                        Parse(item, element);
                    }

                    AddProperty(parent, node.Name.ToString(), item);
                }
            }
            else
            {
                AddProperty(parent, node.Name.ToString(), node.Value.Trim());
            }
        }

        private static void AddProperty(dynamic parent, string name, object value)
        {
            if (parent is List<dynamic>)
            {
                (parent as List<dynamic>).Add(value);
            }
            else
            {
                (parent as IDictionary<String, object>)[name] = value;
            }
        }
    }
    public class data{
        public string USER { set; get; }
        public string PASS { set; get; }
        public string PLAYER { set; get; }
        public string ID_UNICO { set; get; }
    }
    public class recibir{
        public string PLAYER_ID { set; get; }
        public decimal MONTO { set; get; }
        public string OBSERVACION { set; get; }
    }

    public class responder
    {
        public Boolean ALLOWED { set; get; }
        public string SALDO { set; get; }
        public string OBSERVACION { set; get; }
        public long TOKEN { set; get; }


    }
    public class responder2
    {
        public string ALLOWED { set; get; }
  
        public string OBSERVACION { set; get; }

        public decimal MONTO { set; get; }


    }
    public class ipremiar{
        public long TOKEN { set; get; }
        public string PLAYER_ID { set; get; }
        public string OBSERVACION { set; get; }
        public decimal MONTO { set; get; }
        public string WAGER_ID { set; get; }

    }
    public class responder1
    {
        public string RECIBIDO { set; get; }

        public string OBSERVACION { set; get; }


    }
    public class iloginsp {
        public string USER { set; get; }
        public string PASS { set; get; }
        public string PLAYER_ID { set; get; }
        public string SALDO { set; get; }
    }
    public class postfields {
        public data arg { set; get; }
    }
    public class poststring{
       public postfields data1 = new postfields();
        public void init()
        {
            data1.arg = new data();
        }
 

 
        public string postJson()
        {
            return "arg="+Newtonsoft.Json.JsonConvert.SerializeObject(data1.arg) ;
        }
    }
}