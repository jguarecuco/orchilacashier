﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrchilaFinal.Models
{
	public class ipoker
		{
		public long operationtype { set; get; }
		public int providerid { set ; get; }
		public int userid { set; get; }
		public int gameid { set; get; }
		public double amount { set; get; }
		public string transferid { set; get; }
		public string note { set; get; }
		public string sessionid { set; get; }

		public string md5 { set; get; }
		}
    public class iSaldos
    {
        public long id { set; get; }
        public decimal Caja { set; get; }
        public decimal Casino { set; get; }
        public decimal Sportsbook { set; get; }
        public decimal Total { set; get; }
        public string Nombre { set; get; }
        public string Apellido { set; get; }
        public string Email { set; get; }
        public string Cedula { set; get; }
        public string Usuario { set; get; }
    }
    public class iPlayerinfo
    {
        public string status { set; get; }
        public string balance { set; get; }
        public string bonusbalance { set; get; }
        public string currentbet { set; get; }
        public string declinedwdbonuses { set; get; }
        public string viplevel { set; get; }

    }
    public class bancos
    {
        public long id { set; get; }
        public string banco { set; get; }
    }
    public class iresult {
        public string sucess { set; get; }
        public string msg { set; get; }

    }
    public class iresult1
    {
        public string success { set; get; }
        public string msg { set; get; }

    }
}