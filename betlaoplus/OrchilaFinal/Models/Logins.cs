﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrchilaFinal.Models
{
    public class Logins
    {

        public string result { set; get; }
        public string usuario { set; get; }
        public string msg { set; get; }
        public string password { set; get; }
        public decimal cajaprincipal { set; get; }
        
    }
    public class Login
    {
        public string Username { set; get; }
        public string Password { set; get; }
    }
	public class Login_token
		{
		public string Username { set; get; }
		public string Password { set; get; }
		public DateTime Fecha { set;get; }
		public string tokensportbook { set;get; }
		public string tokenBOP { set;get; }
		}
}