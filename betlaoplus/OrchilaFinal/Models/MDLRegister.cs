﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace OrchilaFinal.Models
	{
	public class MDLRegister
		{
		public string Name { set; get; }
		public string Apellido { set; get; }
		public string Email { set; get; }
		public string Email_confirm { set; get; }
		public string Cedula { set; get; }
		public DateTime Fecha { set; get; }
		public string Pais { set; get; }
		public string Estado { set; get; }
		public string Telefono { set; get; }
		public string Pregunta { set; get; }
		public string Respuesta { set; get; }
		public string Entero { set; get; }
		public string Referidos { set; get; }
		public bool EsMayor { set; get; }
		}
	}
