/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
var opts = {};
var resources = [
];
var symbols = {
"stage": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
            {
                id: 'spritte1',
                type: 'image',
                rect: ['0', '0','9030px','1900px','auto', 'auto'],
                clip: ['rect(0px 1290px 945px 0px)'],
                fill: ["rgba(0,0,0,0)",im+"spritte1.jpg",'0px','0px']
            },
            {
                id: 'cortado2',
                type: 'audio',
                tag: 'audio',
                rect: ['0', '0','320px','45px','auto', 'auto'],
                source: ['media/cortado2.mp3']
            },
            {
                id: 'cortado1',
                type: 'audio',
                tag: 'audio',
                rect: ['0', '0','320px','45px','auto', 'auto'],
                source: ['media/cortado1.mp3']
            }],
            symbolInstances: [

            ]
        },
    states: {
        "Base State": {
            "${_Stage}": [
                ["color", "background-color", 'rgba(0,0,0,1.00)'],
                ["style", "overflow", 'hidden'],
                ["style", "height", '600px'],
                ["style", "width", '800px']
            ],
            "${_spritte1}": [
                ["style", "top", '-355px'],
                ["transform", "scaleY", '0.62683'],
                ["transform", "scaleX", '0.62056'],
                ["style", "left", '-1712px'],
                ["style", "clip", [0,1290,945,0], {valueTemplate:'rect(@@0@@px @@1@@px @@2@@px @@3@@px)'} ],
                ["style", "background-position", [0,0], {valueTemplate:'@@0@@px @@1@@px'} ]
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 16000,
            autoPlay: true,
            timeline: [
                { id: "eid23", tween: [ "transform", "${_spritte1}", "scaleY", '0.62683', { fromValue: '0.62683'}], position: 12000, duration: 0 },
                { id: "eid21", tween: [ "transform", "${_spritte1}", "scaleX", '0.62056', { fromValue: '0.62056'}], position: 12000, duration: 0 },
                { id: "eid1", tween: [ "style", "${_spritte1}", "background-position", [0,0], { valueTemplate: '@@0@@px @@1@@px', fromValue: [0,0]}], position: 0, duration: 0 },
                { id: "eid2", tween: [ "style", "${_spritte1}", "background-position", [-1290,0], { valueTemplate: '@@0@@px @@1@@px', fromValue: [0,0]}], position: 1000, duration: 0 },
                { id: "eid3", tween: [ "style", "${_spritte1}", "background-position", [-2580,0], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-1290,0]}], position: 2000, duration: 0 },
                { id: "eid4", tween: [ "style", "${_spritte1}", "background-position", [-3870,0], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-2580,0]}], position: 3000, duration: 0 },
                { id: "eid5", tween: [ "style", "${_spritte1}", "background-position", [-5160,0], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-3870,0]}], position: 4000, duration: 0 },
                { id: "eid6", tween: [ "style", "${_spritte1}", "background-position", [-6450,0], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-5160,0]}], position: 5000, duration: 0 },
                { id: "eid7", tween: [ "style", "${_spritte1}", "background-position", [-7740,0], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-6450,0]}], position: 6000, duration: 0 },
                { id: "eid9", tween: [ "style", "${_spritte1}", "background-position", [0,-955], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-7740,0]}], position: 7000, duration: 0 },
                { id: "eid10", tween: [ "style", "${_spritte1}", "background-position", [-1290,-955], { valueTemplate: '@@0@@px @@1@@px', fromValue: [0,-955]}], position: 8000, duration: 0 },
                { id: "eid11", tween: [ "style", "${_spritte1}", "background-position", [-2580,-955], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-1290,-955]}], position: 9000, duration: 0 },
                { id: "eid12", tween: [ "style", "${_spritte1}", "background-position", [-3870,-955], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-2580,-955]}], position: 10000, duration: 0 },
                { id: "eid19", tween: [ "style", "${_spritte1}", "background-position", [-3870,-955], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-6450,-955]}], position: 15000, duration: 0 },
                { id: "eid20", tween: [ "style", "${_spritte1}", "background-position", [-3870,-955], { valueTemplate: '@@0@@px @@1@@px', fromValue: [-5160,-955]}], position: 16000, duration: 0 },
                { id: "eid22", tween: [ "style", "${_spritte1}", "left", '-1712px', { fromValue: '-1712px'}], position: 12000, duration: 0 },
                { id: "eid24", tween: [ "style", "${_spritte1}", "top", '-355px', { fromValue: '-355px'}], position: 12000, duration: 0 },
                { id: "eid13", trigger: [ function executeMediaFunction(e, data) { this._executeMediaAction(e, data); }, ['play', '${_cortado1}', [9] ], ""], position: 0 },
                { id: "eid14", trigger: [ function executeMediaFunction(e, data) { this._executeMediaAction(e, data); }, ['play', '${_cortado2}', [4] ], ""], position: 9000 }            ]
        }
    }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources, opts);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-2046834997");
