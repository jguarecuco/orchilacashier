﻿var er = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$";
var patt = new RegExp(er);

function message(type, tempName) {

    
    $('.errors').empty();
    if (type == 1) //inputs
    {
        if (tempName == "Email" || tempName == "Email_confirm")
            $(".errors").append("Los correos electronicos son requeridos").css({"display": "block","left":"289px"});
        else
            $(".errors").append("El campo " + tempName +" es requerido").css({"display": "block","left":"289px"});
    }
    else if(type== 2)//check
        $(".errors").append("Debes aceptar los términos y condiciones").css({"display": "block","left":"289px"});
    else if(type == 3) // igualdad de los emails 
        $(".errors").append("Los correos deben de ser iguales").css({"display": "block","left":"289px"});
    else if(type == 4) // formato correcto de los Emails
        $(".errors").append("Los correos deben de tener el formato correcto").css({"display":"block","left":"233px"});
}

function validation(object) {
    var band = 0;
    //console.log(object.sort());
    
    

    object.sort(function(input1, input2) {
        return $(input1).data('id') - $(input2).data('id');
    });
    

    
    jQuery.each(object, function (index, inputt) {
        

            if ($(inputt).prop("value") == "" && band == 0) {
                var tempName = $(inputt).attr("name") == "Name" ? "Nombre" : $(inputt).attr("name");
                message(1, tempName);
                band = 1;
                $(inputt).addClass("border-my");
                //return false;
            }


            else if (($(inputt).attr('name') == "EsMayor") && $(inputt).prop("checked") == false && band==0){
                message(2,"");
                band = 1;
                $(inputt).addClass("border-my");
                //return false;
            }

                /*Expresion regular para los Email's */
            else if (($(inputt).attr('name') == "Email" || $(inputt).attr('name') == "Email_confirm") && band == 0) {
				
                if ($(inputt).attr('name') == "Email_confirm" && patt.test($(inputt).val()) && band == 0) {

                    if ($('input[name=Email]').val() != $(inputt).val()) {
                        message(3, "");
                        band = 1;
                        $(inputt).addClass("border-my");
                    }

                }
                else if (!patt.test($(inputt).val())) {
                    message(4, "");
                    band = 1;
                }
				

            }
			
            else if (band == 1 && $(this).val() == "") {
                console.log("entrando aqui");
                $(inputt).addClass("border-my");
            } 
    });
    
    
    return band == 1 ? false : true;
}

$(document).ready(function () {

    

    if ($.trim($('div.errors').html()) != '')
    {
        $('div.errors').css('display', 'block');

    }


    $("#button-close").click(function () {
        $("#u1049", window.parent.document).trigger("click");
    });

    /*EMAILS*/
    $("input#Email, input#Email_confirm").blur(function () {

        if ($(this).attr('name') == "Email" || $(this).attr('name') == "Email_confirm") {
            if (!patt.test($(this).val())) {
                $(this).addClass("border-my");
                message(4, "");
            }
        }
    });

    /*checkbox*/
    $('input#EsMayor').on('change', function () {
        if($(this).prop("checked")){
            $('.errors').fadeOut("slow");
            //$('.errors').empty();
        }
            
    });

    /*Fecha*/
    $('input#Fecha').on('change', function () {
        if ($(this).val() == "")
            $(this).addClass("border-my");
        else {
            $(this).removeClass("border-my");
        }
    });

    /*desaparecer message de error*/
    $('input').keyup(function () {
        if ($(this).val() == "")
            $(this).addClass("border-my");
        else
            $(this).removeClass("border-my");

        if ($('.errors').css("display") == 'block') {
            
            $('.errors').fadeOut("slow");
        }
            
    });

    
    $('input').on('change', function () {
        $('.errors').fadeOut("slow");
    });

    $('#registrar').submit(function (e) {
//        e.preventDefault();

        $('.errors').empty().css("display", "none");

        if (!validation($(this).find('input').not('input[name=Referidos]')))            
            e.preventDefault();
        
    });
    $("#botonBobCalendar").click(function () {
        $("#Fecha").datepicker("show");
    });
    $("#Fecha").datepicker({
 
        changeYear: true,
        yearRange: "1920:1996",
        dateFormat: 'dd/mm/yy'
    });
});