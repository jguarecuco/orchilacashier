﻿using minicashier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace minicashier.Controllers
{

    public class UserRetirarController : ApiController
    {
        public MDLResponse Post([FormBody] MDLRetirar model) {
            

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("token", model.token);
            data.Add("banco", model.banco);
            data.Add("tipo_cuenta", model.tipo_cuenta);
            data.Add("cedula", model.cedula);
            data.Add("cuenta", model.cuenta);
            data.Add("monto", model.monto.ToString());
            

            string respuesta = Utility.HTTPPost(data, "http://www.betlaorchila.com:1850/api/withdraw");
            MDLResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);



            if (String.Compare(response.status, "00") == 0)
            {
                response.result = "OK";
                response.Menssage = "Retiro realizado con exito!";
            }
            else if(String.Compare(response.status, "10") == 0){
                response.result = "FAIL";
                response.Menssage = "El monto a retirar debe de ser mayor a 100";
            }
            else
            {
                response.result = "FAIl";
                response.Menssage = "fallo al retirar!";
            }

            return response;
        }
    }
}
