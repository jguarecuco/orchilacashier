﻿using minicashier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace minicashier.Controllers
{
    public class UserTransferenciaController : ApiController
    {

        public MDLResponse Post([FormBody] MDLTransferencia model)
        {


            //bool validation = ValidationsTransfer(model.origen, model.destino);

            if (ValidationsTransfer(model.origen, model.destino))
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("token", model.token);
                data.Add("cashier_from", model.origen);
                data.Add("cashier_to", model.destino);
                data.Add("amount", model.monto.ToString());

                string respuesta = Utility.HTTPPost(data, "http://www.betlaorchila.com:1850/api/transferbalance");
                MDLResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);

                if (String.Compare(response.status, "00") == 0)
                {
                    response.result = "OK";
                    response.Menssage = "La transferencia se ha realizado con exito!";
                }
                else
                {
                    response.result = "FAIl";
                    response.Menssage = "Transferencia no aprobada!";
                }

                return response;
            }
            else
            {
                MDLResponse respuesta = new MDLResponse();
                respuesta.Menssage = "Problemas al transferir!";
                respuesta.status = "007";
                respuesta.result = "";
                return respuesta;
            }
            
        }

        public static bool ValidationsTransfer(string origen, string destino)
        {
            switch (origen)
            { 
                case "1":
                    switch (destino) 
                    {
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                            return true;
                        default:
                            return false;
                    }
                
                case "2":
                case "3":
                case "4":
                case "5":
                    if (String.Compare(destino, "1") == 0)
                        return true;
                    else
                        return false;

                default:
                    return false;

            }
            
        }
    }
}
