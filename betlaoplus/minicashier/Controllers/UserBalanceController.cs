﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using System.Net.Http;
using System.Web.Http;
using minicashier.Models;


namespace minicashier.Controllers
{
    public class UserBalanceController : ApiController
    {
        public betdevEntities db = new betdevEntities();
		public MDLBalance Post([FromBody] string token) {
            /*BO*/
            //session_integrator session = db.session_integrator.Single(f => f.Token == token);
            Dictionary<string,string> data = new Dictionary<string,string>();
            data.Add("token",token);

            string respuesta = Utility.HTTPPost(data, "http://www.betlaorchila.com:1850/api/balance");
            MDLResponse saldos = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);
            

			MDLBalance valores=new MDLBalance();
            valores.caja = String.Format("{0:N}", (decimal)saldos.result.main_balance);
			valores.casino = String.Format("{0:N}",(decimal)saldos.result.casino_balance);
            valores.betlaoplus = String.Format("{0:N}", (decimal)saldos.result.boplus_balance);
            valores.sportbook = String.Format("{0:N}",(decimal)saldos.result.sportsbook_balance);
            valores.poker = String.Format("{0:N}", (decimal)saldos.result.poker_balance); //salos.result.poker_balance
            valores.total = String.Format("{0:N}",(decimal)saldos.result.total_balance);
			return valores;
		}
    }
}
