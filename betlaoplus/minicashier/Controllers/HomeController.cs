﻿using minicashier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace minicashier.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public betdevEntities db = new betdevEntities();
        public ActionResult Index(string id)
        
        {


            try
            {
                
                
                Dictionary<string, string> data = new Dictionary<string, string>();
                
                data.Add("token", id);

                string respuesta = Utility.HTTPPost(data, "http://www.betlaorchila.com:1850/api/profiler");
                MDLResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);
                bo_form_register usuario = new bo_form_register();

                if (String.Compare(response.status, "00") == 0)
                {

                    usuario.name = response.result.name;
                    usuario.apellido = response.result.second_name;
                    usuario.ci = response.result.identification_id;
                    usuario.email = response.result.acount_emails;
                    usuario.login = response.result.username;

                    ViewBag.token = id;
                    var mytoken = id;
                    string respuesta1 = Utility.HTTPPost(data, "http://www.betlaorchila.com:1850/api/balance");
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta1);

                    MDLBalance valores = new MDLBalance();
                    valores.caja = String.Format("{0:N}", (decimal)response.result.main_balance);
                    valores.casino = String.Format("{0:N}", (decimal)response.result.casino_balance);
                    valores.betlaoplus = String.Format("{0:N}", (decimal)response.result.boplus_balance);
                    valores.sportbook = String.Format("{0:N}", (decimal)response.result.sportsbook_balance);
                    valores.poker = String.Format("{0:N}", (decimal)response.result.poker_balance);
                    valores.total = String.Format("{0:N}", (decimal)response.result.total_balance);

                    ViewBag.CajaPrincipal = valores.caja;
                    ViewBag.Casino = valores.casino;
                    ViewBag.Betlaorchila = valores.betlaoplus;
                    ViewBag.Sportbook = valores.sportbook;
                    ViewBag.poker = valores.poker;
                    ViewBag.Total = valores.total;
                    
                    return View(usuario);
                }
                else if (String.Compare(response.status, "01") == 0)
                {
                    return RedirectToAction("Error");
                }

                return View(usuario);
                
               
            }
            catch (Exception e) 
            {
                return RedirectToAction("Error");
            }
            
            
        }


		 
        /*
        public string getToken() {
            return Session["token"].ToString();
        } */
        public ActionResult Error() {
            return View();
        }
    }
}
