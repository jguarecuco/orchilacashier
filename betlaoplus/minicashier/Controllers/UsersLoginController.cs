﻿using minicashier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace minicashier.Controllers
{
    public class UsersLoginController : ApiController
    {
	public betdevEntities  db=new betdevEntities();
		public dynamic Post([FromBody] mdlLogin model) { 
				  var users=db.bo_form_register.Where(f=>f.login == model.username & f.org_id == 1).ToList().Where(f=>f.password==model.password).ToList();
				  if (users.Count > 0) {
                  
				  string cadena = model.username + "|" + model.password + "|" + DateTime.Now.Ticks.ToString( );	
				  response resp=new response();
				  resp.REST="OK";
				  resp.token= Convert.ToBase64String(CommonFunction.Encryptor(cadena));
				  return resp;
				  } else {
				  response resp = new response( );
				  resp.REST = "FAIL";
				  resp.token="";
				  return resp;
				  }
		}

        public dynamic Post([FromBody] mdlPerfil model) 
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("token", model.token);

            string respuesta =  Utility.HTTPPost(data,"http://www.betlaorchila.com:1850/api/apiprofiler");
            MDLResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);

            return response;

        }
    }
}
