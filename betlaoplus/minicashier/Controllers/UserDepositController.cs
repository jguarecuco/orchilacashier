﻿using minicashier.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace minicashier.Controllers
{
    public class UserDepositController : ApiController
    {
        public MDLResponse Post([FromBody] MDLTransaction model) 
        {
            

            MDLResponse res = new MDLResponse();
            if (model.amount > 0)
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("token", model.token);
                data.Add("amount", model.amount.ToString());
                data.Add("currency", "Bs.");
                data.Add("bankname", model.banco);
                data.Add("reference", model.n_transation.ToString());
                data.Add("deposit_date", model.fecha);

                string respuesta = Utility.HTTPPost(data, "http://www.betlaorchila.com:1850/api/deposit");
                MDLResponse response = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);


                if(String.Compare(response.status, "00") == 0)
                {
                    response.result = "OK";
                    response.Menssage = "El deposito se ha realizado con exito!";
                }
                else
                {
                    response.result = "FAIl";
                    response.Menssage = "fallo al depositar!";
                }
                return response;
            }
            else {
                
                res.result = "FAIL";
                res.Menssage = "El monto es menor a 0";
                res.status = "";
                return res;
            }

        }


    }
}
