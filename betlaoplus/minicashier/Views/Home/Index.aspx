﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<minicashier.Models.bo_form_register>" %>

<!DOCTYPE html>

<html ng-app="laorchila">
<head runat="server">
    <meta name="viewport" content="width=device-width" />

    <meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<meta name="author" content="">
				<title>Caja</title>
				
                <link href="<%:Url.Content("~/")%>Contenido/accest/img/favicon.ico" rel="shortcut icon" />

				<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">-->
                <link href="<%:Url.Content("~/")%>Contenido/accest/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
				<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">-->
                <link href="<%:Url.Content("~/")%>Contenido/accest/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />

                <link rel="stylesheet" href="<%:Url.Content("~/") %>Contenido/accest/css/bootstrap-select.min.css">
				<link rel="stylesheet" href="<%:Url.Content("~/") %>Contenido/accest/css/bootstrap-datetimepicker.min.css">
				<link rel="stylesheet" href="<%:Url.Content("~/") %>Contenido/accest/css/datepicker.css">
				<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
                <link rel="stylesheet"  href="<%:Url.Content("~/") %>Contenido/accest/css/jquery-ui.css">
                <link rel="stylesheet"  href="<%:Url.Content("~/") %>Contenido/accest/css/screen.css">
                <link rel="stylesheet"  href="<%:Url.Content("~/") %>Contenido/accest/css/cmxform.css">
                <link rel="stylesheet"  href="<%:Url.Content("~/") %>Contenido/accest/ladda/dist/ladda-themeless.min.css">
                
				<link rel="stylesheet"  href="<%:Url.Content("~/") %>Contenido/accest/css/style.css">

                <!--<script src="<%:Url.Content("~/") %>Scripts/angular.min.js"></script>-->
                <script>
                    window.token = "<%:ViewBag.token%>";
                </script>

</head>
<body class="img-responsive" >
    
				<div class="logo"></div>

				<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
					<div class="container">
                        <div class="my-header">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-up-navbar-collapse-1">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
						    </button>
							<a class="navbar-brand" href="#index" id="log-betla" aria-controls="index" role="tab" data-toggle="tab"><img src="<%:Url.Content("~/") %>Contenido/accest/img/logo-orchila-resp.png"></a>
						</div>

								<div class="collapse navbar-collapse" id="bs-up-navbar-collapse-1">
									<ul class="nav navbar-nav" role="tablist">
                                        <li role="presentation"><a href="#index" id="user" aria-controls="index" role="tab" data-toggle="tab">Usuario: <%:Model.login %></a></li>
										<li role="presentation"><a href="#bancos" aria-controls="bancos" role="tab" data-toggle="tab">Bancos</a></li>
										<li role="presentation"><a href="#depositos" aria-controls="depositos" role="tab" data-toggle="tab">Depositos</a></li>
										<li role="presentation"><a href="#transferencias" aria-controls="transferencias" role="tab" data-toggle="tab">Transferencias</a></li>
										<li role="presentation"><a href="#retiros" aria-controls="retiros" role="tab" data-toggle="tab">Retiros</a></li>
									</ul>
								</div>
                            </div>
					</div>
				</nav>
                

  

				<div class="container-fluid gral">
                    <div class="tab-content">

                    <!--INDEX-->
                    <div role="tabpanel" class="tab-pane active" id="index">
					<div class="row sub-row">
						<div class="col-sm-1 col-md-3"></div>
						<div class="col-sm-10 col-md-6">
							<div class="cont-general">
								<div class="info-user">
                                    <form class="my-form">

									<table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Nombre</td>
                                              </tr>
											  <tr role="myindex" >     
												<td scope="col" class="table-td only-td">Nombre:&nbsp; </td>
												<td scope="col" colspan="2"><input id="nombre" type="text" size="1" class="form-control static" placeholder="" aria-invalid="false" readonly value="<%:Model.name%>"></td>
											  </tr>
										  </tbody>
									  </table>
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Apellido</td>
                                              </tr>
											  <tr role="myindex">
												<td scope="col" class="table-td only-td">Apellido:&nbsp; </td>
												<td scope="col" colspan="2"><input id="apellido" type="text" size="1" class="form-control static" placeholder="" aria-invalid="false" readonly value="<%:Model.apellido %>"></td>
											  </tr>
										  </tbody>
									  </table>
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Correo Electrónico</td>
                                              </tr>
											  <tr role="myindex">
												<td scope="col" class="table-td only-td">Correo Electrónico: &nbsp; </td>
												<td scope="col" colspan="2"><input id="correo" type="text" size="1" class="form-control static" placeholder="" aria-invalid="false" readonly value="<%:Model.email %>"></td>
											  </tr>
										  </tbody>
									  </table>
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Cédula</td>
                                              </tr>
											  <tr role="myindex">
												<td scope="col" class="table-td only-td">Cedula:&nbsp; </td>
												<td scope="col" colspan="2"><input id="cedula" type="text" size="1" class="form-control static" placeholder="" aria-invalid="false" readonly value="<%:Model.ci %>"></td>
											  </tr>
										  </tbody>
									  </table>
								    </form>	
								</div>
                                
							</div>
							
						</div>
						<div class="col-sm-1 col-md-3"></div>
					</div>

                    </div>

                    <!--BANCO-->
                <div role="tabpanel" class="tab-pane" id="bancos">
                    <div class="row sub-row">
						<div class="col-xs-1 col-sm-2 col-md-3"></div>
						<div class="col-xs-10 col-sm-8 col-md-6">
							<div class="cont-general sub-cont-gral">
								<div class="logo-bancos"> 
									<div class="bancos banco-uno img-responsive"></div>
									<div class="bancos banco-dos img-responsive"></div>
									<div class="bancos banco-tres img-responsive"></div>
									<div class="bancos banco-cuatro img-responsive"></div>
									<div class="bancos banco-cinco img-responsive"></div>
									<div class="bancos sub-linea banco-seis"></div>
								</div>
								<div class="nuemeros-cuentas">
									<div class="cuentas">
										<p class="titulos-cuentas"> <strong>Banco Occidental de Descuento </strong></p>
										<p class="numeros-cuentas"> <strong>01160141320014091046</strong> </p>
									</div>
									<div class="cuentas">
										<p class="titulos-cuentas"> <strong>Banesco </strong></p>
										<p class="numeros-cuentas"> <strong>01340453434531036919</strong> </p>
									</div>
									<div class="cuentas">
										<p class="titulos-cuentas"> <strong>BBVA Banco Provincial </strong></p>
										<p class="numeros-cuentas"> <strong>01080300440100088097</strong> </p>
									</div>
									<div class="cuentas">
										<p class="titulos-cuentas"> <strong>Banco De Venezuela </strong></p>
										<p class="numeros-cuentas"> <strong>01020748740000013356</strong> </p>
									</div>
									<div class="cuentas">
										<p class="titulos-cuentas"> <strong>Banco Sofitasa </strong></p>
										<p class="numeros-cuentas"> <strong>01370038970000099281</strong> </p>
									</div>
									<div class="cuentas">
										<p class="titulos-cuentas"> <strong>Bancaribe </strong></p>
										<p class="numeros-cuentas"> <strong>01140560665600034318</strong> </p>
									</div>
								</div>
							</div>
							
						</div>
						<div class="col-xs-1 col-sm-2 col-md-3"></div>
					</div>
                    </div>

                    <!--DEPOSITOS-->
                    <div role="tabpanel" class="tab-pane" id="depositos">
                    <div class="row sub-row">
						<div class="col-sm-2 col-md-3 col-lg-3"></div>
						<div class="col-sm-8 col-md-6 col-lg-6">
							<div class="cont-general">
								<div class="info-user">
                                    <form id="deposito" class="my-form">
                                        
									<table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Banco</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Banco:&nbsp; </td>
												<td scope="col" colspan="2">
													<select class="selectpicker" id="banco">
														<option>Banesco</option>
														<option>Occidental de descuento (B.O.D)</option>
														<option>Provincial</option>
														<option>Venezuela</option>
														<option>Sofitasa</option>
														<option>Bancaribe</option>
														<option class="divider"></option>
														<option>Canjea tu bono</option>
													</select>
												</td>
											  </tr>
										  </tbody>
									  </table>
									  
                                      <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Transacción</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Transacción N°: &nbsp; </td>
												<td scope="col" class="columna" colspan="2"><input type="text" id="transaccion" name="transaccion" onkeypress='return event.charCode >= 48 && event.charCode <= 57' size="1" class="form-control" placeholder="" aria-invalid="false" maxlength="25" form="deposito"></td>
											  </tr>
										  </tbody>
									  </table>
									 
                                      <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Monto</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Monto: &nbsp; </td>
												<td scope="col" colspan="2"><input type="text" id="monto" name="monto" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46' size="1" class="form-control" placeholder="" aria-invalid="false" maxlength="25" form="deposito" ></td>
											  </tr>
										  </tbody>
									  </table>
									  
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
											<tbody>
                                                <tr class="names-responsive">
                                                  <td>Fecha</td>
                                              </tr>
												  <tr role="presentation">
													<td scope="col" class="table-td">Fecha: &nbsp; </td>
													<td scope="col" colspan="2" ><input type="text" id="datemar" name="fecha" class="estilo" readonly form="deposito" ></td>
												  </tr>
											  </tbody>					  	
									  </table>
									  <br>
									  <br>
									  <button type="submit" id="deposito" class="btn btn-success my-button ladda-button" data-style="zoom-in">Enviar Deposito</button>
                                     </form>
								</div>
                                
							</div>
							
						</div>
						<div class="col-sm-2 col-md-3 col-lg-3"></div>
					</div>
                    </div>    

                    <!--TRANSFERENCIAS-->
                    <div role="tabpanel" class="tab-pane" id="transferencias">
                    <div class="row sub-row">
						<div class="col-sm-2 col-md-3"></div>
						<div class="col-sm-8 col-md-6">
							<div class="cont-general">
								<div class="info-user">
                                    <form id="formtransf" class="my-form">
									<table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Origen</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Origen:&nbsp; </td>
												<td scope="col" colspan="2">
													<select class="selectpicker" id="origen">
														<option value=1>Caja Principal</option>
														<option value=2>Casino</option>
														<option value=3>Sportbook</option>
                                                        <!--<option value=4>Betlaorchila plus</option>-->
                                                        <option value=5>Poker</option> 
													</select>
												</td>
											  </tr>
										  </tbody>
									  </table>
									  
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Destino</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Destino:&nbsp; </td>
												<td scope="col" colspan="2">
													<select class="selectpicker" id="destino">
														<option value=2>Casino</option>
														<option value=3>Sportbook</option>
                                                        <!--<option value=4>Betlaorchila plus</option>-->
                                                        <option value=5>Poker</option>
													</select>
												</td>
											  </tr>
										  </tbody>
									  </table>
									 
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Monto</td>
                                              </tr>
											  <tr role="presentation" style="width:100%;">
												<td scope="col" class="table-td">Monto: &nbsp; </td>
												<td scope="col" colspan="2" ><input type="text" id="monto3" name="monto" form="formtransf" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46' size="1" class="form-control" placeholder="" aria-invalid="false" maxlength="25"></td>
											  </tr>
										  </tbody>
									  </table>
									  <br>
									  <br>
									  <button type="submit" id="transferir" class="btn btn-success my-button ladda-button" data-style="zoom-in">Transferir</button>
                                    </form>
								</div>
							</div>
							
						</div>
						<div class="col-sm-2 col-md-3"></div>
					</div>
                   </div> 

                   <!--RETIRO-->
                    <div role="tabpanel" class="tab-pane" id="retiros">
                    <div class="row sub-row">
						<div class="col-sm-2 col-md-3"></div>
						<div class="col-sm-8 col-md-6">
							<div class="cont-general">
								<div class="info-user">
                                    <form id="retirar" class="my-form">
									<table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Banco</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Banco:&nbsp; </td>
												<td scope="col" colspan="2">
													<select class="selectpicker" id="banco2" placeholder="">
														<option>Banesco</option>
														<option>Occidental de descuento (B.O.D)</option>
														<option>Provincial</option>
														<option>Venezuela</option>
														<option>Sofitasa</option>
														<option>Bancaribe</option>
														<option class="divider"></option>
														<option>Canjea tu bono</option>
													</select>
												</td>
											  </tr>
										  </tbody>
									  </table>
									  
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Tipo de Cuenta</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Tipo de Cuenta:&nbsp; </td>
												<td scope="col" colspan="2">
													<select class="selectpicker" id="tipo_cuenta" placeholder="">
														<option>Corriente</option>
														<option>Ahorro</option>
													</select>
												</td>
											  </tr>
										  </tbody>
									  </table>
									  	
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Cédula o ID</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Cedula o ID: &nbsp; </td>
												<td scope="col" colspan="2"><input id="cedula2" name="cedula" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' size="1" class="form-control" placeholder="" aria-invalid="false" maxlength="25"></td>
											  </tr>
										  </tbody>
									  </table>
									  
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Cuenta Bancaria</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Cuenta Bancaria: &nbsp; </td>
												<td scope="col" colspan="2" ><input type="text" id="cuenta2" name="cuenta bancaria" onkeypress='return event.charCode >= 48 && event.charCode <= 57' size="1" class="form-control" placeholder="" aria-invalid="false" maxlength="25"></td>
											  </tr>
										  </tbody>
									  </table>
									   
									  <table border="0" cellspacing="0" cellpadding="000" class="my-input">
										  <tbody>
                                              <tr class="names-responsive">
                                                  <td>Monto</td>
                                              </tr>
											  <tr role="presentation">
												<td scope="col" class="table-td">Monto: &nbsp; </td>
												<td scope="col" colspan="2" ><input type="text" id="monto2" name="monto" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46' size="1" class="form-control" placeholder="" aria-invalid="false" maxlength="25"></td>
											  </tr>
										  </tbody>
									  </table>
									  <br>
									  <br>
									  <button type="submit" id="retiro" class="btn btn-success my-button ladda-button" data-style="zoom-in">Enviar Retiro</button>
                                    </form>
								</div>
							</div>
							
						</div>
						<div class="col-sm-2 col-md-3"></div>
					</div>
				</div>                 
                    </div> <!--fin tab panes-->
                    </div>

    
     <div class="render" style="font-family:'Abel';  visibility:hidden;">p</div>
     <div class="render" style="font-family:'Glyphicons Halflings'; visibility:hidden;">p</div>
     <div class="render" style="background-image:url(/Contenido/accest/img/check.png); visibility: hidden;"></div>
     <div class="render" style="background-image:url(/Contenido/accest/img/cancel.png); visibility: hidden;"></div>
     <div class="render" style="background-image:url(/Contenido/accest/img/aceptarmodal-normal.png); visibility:hidden;"></div>
     <div class="render" style="background-image:url(/Contenido/accest/img/aceptarmodal-hover.png); visibility:hidden;"></div>
    
            
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Titulo del mensaje</h4>
                  </div>
                  <div class="modal-body">
                    Aqui va el mensaje BOB, mi copion amigo bob....
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default boton-aceptar-modal" data-dismiss="modal"></button>
                  </div>
                </div>
              </div>
            </div>
    				
				
					    <div class="container-fluid my-container">
						    <div class="row">

                                <nav class="navbar navbar-inverse navbar-bottom my-navar" role="navigation">
                                    <div class="navbar-header">
							            <button type="button" id="see-balance" class="navbar-toggle my-button-up" data-toggle="collapse" data-target="#bs-bottom-navbar-collapse">
						                </button>
						            </div>
                                    

                                    <div class="collapse navbar-collapse" id="bs-bottom-navbar-collapse">
                                        <div class="col-md-0"></div>
                                        <div class="col-md-12">
                                         <ul class="nav navbar-nav my-list">
                                             <li role="presentation">
							                    <div class="padd borde-right div-hidden other">
								                    <div class="div-int">
									                    <div class="titulos"><h4 class="h4edit text-right"><strong>Caja Principal</strong></h4></div>
									                    <div class="info"><h4 class="h4edit text-right"><strong id="caja"><%: ViewBag.CajaPrincipal %></strong></h4></div>
                                                        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate loading-amount"></span>
								                    </div>
							                    </div>
                                            </li>
                                            <li role="presentation">
							                    <div class="padd div-hidden other">
								                    <div class="div-int">
									                    <div class="titulos"><h4 class="h4edit text-right"><strong>Casino</strong></h4></div>
									                    <div class="info"><h4 class="h4edit text-right"><strong id="casino"><%: ViewBag.Casino %></strong></h4></div>
                                                        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate loading-amount"></span>
								                    </div>
							                    </div>
                                            </li>
                                            <li  role="presentation">
							                    <div class="padd div-hidden other">
								                    <div class="div-int">
									                    <div class="titulos"><h4 class="h4edit text-right"><strong>Sportsbook</strong></h4></div>
									                    <div class="info"><h4 class="h4edit text-right"><strong id="sportbook"><%: ViewBag.Sportbook %></strong></h4></div>
                                                        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate loading-amount"></span>
								                    </div>
							                    </div>
                                            </li>
                                            <li role="presentation">
							                    <div class="padd other">
								                    <div class="div-int">
									                    <div class="titulos"><h4 class="h4edit text-right"><strong>Poker</strong></h4></div>
									                    <div class="info"><h4 class="h4edit text-right"><strong id="poker"><%: ViewBag.poker %></strong></h4></div>
                                                        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate loading-amount"></span>
								                    </div>
							                    </div>
                                            </li>
                                             <li role="presentation">
							                    <div class="padd other">
								                    <div class="div-int">
									                    <div class="titulos"><h4 class="h4edit text-right"><strong>Total Disponible</strong></h4></div>
									                    <div class="info"><h4 class="h4edit text-right"><strong id="total"><%: ViewBag.Total %></strong></h4></div>
                                                        <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate loading-amount"></span>
								                    </div>
							                    </div>
                                            </li>

                                        </ul>
                                        </div>
                                        <div class="col-md-0"></div>
                                    </div>
                                    
                                </nav>
                                
                                </div>

					  </div>
        		
					
				
				<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
                <script src="<%:Url.Content("~/")%>Contenido/accest/js/jquery.min.js"></script>
                <script src="<%:Url.Content("~/") %>Contenido/accest/js/jquery.js"></script>
				<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
                <script src="<%:Url.Content("~/")%>Contenido/accest/bootstrap/js/bootstrap.min.js"></script>
				<script src="<%:Url.Content("~/") %>Contenido/accest/js/bootstrap-select.js"></script>
				<script src="<%:Url.Content("~/") %>Contenido/accest/js/moment.min.js"></script>
				<script src="<%:Url.Content("~/") %>Contenido/accest/js/transition.js"></script>
				<script src="<%:Url.Content("~/") %>Contenido/accest/js/jquery.ui.core.js"></script>
       			<script src="<%:Url.Content("~/") %>Contenido/accest/js/jquery.ui.widget.js"></script>
        		<script src="<%:Url.Content("~/") %>Contenido/accest/js/jquery.datepicker.js"></script>
     			<script src="<%:Url.Content("~/") %>Contenido/accest/js/jquery.datepicker.es.js"></script>
                <script src="<%:Url.Content("~/") %>Contenido/accest/js/jquery-ui.js"></script>
                <script src="<%:Url.Content("~/") %>Contenido/accest/js/jquery.validate.js"></script>
                    
                <script src="<%:Url.Content("~/") %>Contenido/accest/ladda/dist/spin.min.js"></script>
                <script src="<%:Url.Content("~/") %>Contenido/accest/ladda/dist/ladda.min.js"></script>

				<script src="<%:Url.Content("~/") %>Contenido/accest/js/principalscript.js"></script>

              
				 <script>

				     $('.selectpicker').selectpicker({
				         size: 4
				     });
				</script>
				
				
					<script type="text/javascript">


					    //este es el scrip para el datepicker

					    $(function () {

					        $("#datemar").datepicker({
					            showOn: "button",
					            changeYear: true,
					            yearRange: "2014:2015",
					            buttonImage: "http://www.betlaorchila.com/images/boton-calendar.png",
					            dateFormat: 'dd/mm/yy',
					            buttonImageOnly: true,
					            maxDate: new Date()
					        });
					    });



					    //este es el scrip para evitar letras en el input de monto y N° transaccion

					    $(document).ready(function () {
					        $("#txtboxToFilter").keydown(function (e) {

					            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||

                                    (e.keyCode == 65 && e.ctrlKey === true) ||

                                    (e.keyCode == 67 && e.ctrlKey === true) ||

                                    (e.keyCode == 88 && e.ctrlKey === true) ||

                                    (e.keyCode >= 35 && e.keyCode <= 39)) {

					                return;
					            }

					            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					                e.preventDefault();
					            }
					        });
					    });


					    $('button#prueba-spy').click(function () {
					        
					        console.log(this);
					        var l = Ladda.create(this);

					        l.start();

					       

					    });

				</script>
</body>
</html>
