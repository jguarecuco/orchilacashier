﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<minicashier.Models.bo_form_register>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Error</title>


    <link href="<%:Url.Content("~/")%>Contenido/accest/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<%:Url.Content("~/")%>Contenido/accest/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="<%:Url.Content("~/") %>Contenido/accest/css/styleError.css">
</head>
<body>
    <div class="container">
        <div class="row my-row">
            <div class="col-xs-12 col-md-5 col-md-push-4 alert-my" align="center">
                <h2>¡Atención!</h2>
                <p class="message">Disculpe pero su sesión ha caducado</p>
                <img src="<%:Url.Content("~/")%>Contenido/accest/img/alert.png" alt="<%:Url.Content("~/")%>Contenido/accest/img/alert.png" />
            </div>
        </div>
    </div>
    
    <script src="<%:Url.Content("~/")%>Contenido/accest/js/jquery.min.js"></script>
    <script src="<%:Url.Content("~/")%>Contenido/accest/bootstrap/js/bootstrap.min.js"></script>


</body>
</html>
