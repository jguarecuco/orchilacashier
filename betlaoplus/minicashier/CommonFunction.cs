﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace minicashier
	{
	public class CommonFunction
		{

		public static byte[] Encryptor(string str)
			{
			string EncrptKey = "lh1414**";
			byte[] byKey = { };
			byte[] IV = { 18 , 52 , 86 , 120 , 144 , 171 , 205 , 239 };
			byKey = System.Text.Encoding.UTF8.GetBytes(EncrptKey.Substring(0 , 8));
			DESCryptoServiceProvider des = new DESCryptoServiceProvider( );
			byte[] inputByteArray = Encoding.UTF8.GetBytes(str);
			MemoryStream ms = new MemoryStream( );
			CryptoStream cs = new CryptoStream(ms , des.CreateEncryptor(byKey , IV) , CryptoStreamMode.Write);
			cs.Write(inputByteArray , 0 , inputByteArray.Length);
			cs.FlushFinalBlock( );
			return ms.ToArray( );
			}

		public static string Decryptor(string str)
			{
			str = str.Replace(" " , "+");
			string DecryptKey = "lh1414**";
			byte[] byKey = { };
			byte[] IV = { 18 , 52 , 86 , 120 , 144 , 171 , 205 , 239 };
			byte[] inputByteArray = new byte[str.Length];
			byKey = System.Text.Encoding.UTF8.GetBytes(DecryptKey.Substring(0 , 8));
			DESCryptoServiceProvider des = new DESCryptoServiceProvider( );
			inputByteArray = Convert.FromBase64String(str.Replace(" " , "+"));
			MemoryStream ms = new MemoryStream( );
			CryptoStream cs = new CryptoStream(ms , des.CreateDecryptor(byKey , IV) , CryptoStreamMode.Write);
			cs.Write(inputByteArray , 0 , inputByteArray.Length);
			cs.FlushFinalBlock( );
			System.Text.Encoding encoding = System.Text.Encoding.UTF8;
			return encoding.GetString(ms.ToArray( ));
			}
		}

	public static class HttpRequestMessageExtensions
		{
		private const string HttpContext = "MS_HttpContext";
		private const string RemoteEndpointMessage =
			"System.ServiceModel.Channels.RemoteEndpointMessageProperty";
		private const string OwinContext = "MS_OwinContext";
		public static string GetClientIpAddress(this HttpRequestMessage request)
			{
			if (request.Properties.ContainsKey(HttpContext))
				{
				dynamic ctx = request.Properties[HttpContext];
				if (ctx != null)
					{
					return ctx.Request.UserHostAddress;
					}
				}
			if (request.Properties.ContainsKey(RemoteEndpointMessage))
				{
				dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
				if (remoteEndpoint != null)
					{
					return remoteEndpoint.Address;
					}
				}
			if (request.Properties.ContainsKey(OwinContext))
				{
				dynamic owinContext = request.Properties[OwinContext];
				if (owinContext != null)
					{
					return owinContext.Request.RemoteIpAddress;
					}
				}
			return null;
			}
		}
	}