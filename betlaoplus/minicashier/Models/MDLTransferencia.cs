﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace minicashier.Models
{
    public class MDLTransferencia
    {
        public string token { set; get; }
        public string origen { set; get; }
        public string destino { set; get; }
        public decimal monto { set; get; }
    }
}