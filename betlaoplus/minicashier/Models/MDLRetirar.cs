﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace minicashier.Models
{
    public class MDLRetirar
    {
        public string token { set; get; }
        public string banco {set; get;}
        public string tipo_cuenta {set; get;}
        public string cedula {set; get;}
        public string cuenta { set; get; }
        public decimal monto { set; get; }

    }
}