﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace minicashier.Models
{
    public class MDLTransaction
    {
        public string token { set; get; }
        //public string masterkey { set; get; }
        public string banco { set; get; }
        public decimal amount { set; get; }
        public decimal n_transation { set; get; }
        public string fecha { set; get; }
    }
    public class MDLResponse 
    {
        public dynamic result { set; get; }
        public string Menssage { set; get; }
        public string status { set; get; }
        
    }
}