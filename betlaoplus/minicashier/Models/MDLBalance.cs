﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace minicashier.Models
	{
	public class MDLBalance
		{
		public string casino { set;get; }
		public string caja { set;get;}
		public string betlaoplus { set;get;}
		public string sportbook { set;get;}
        public string poker { set; get; }
		public string total { set;get; }
		}

	public class MDLDeposito
		{
		public string banco { set;get;}
		public string monto { set;get;}
		public DateTime fecha { set;get;}
		public string token { set;get;}
		}

	}
