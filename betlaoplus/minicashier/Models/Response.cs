﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace minicashier.Models
	{
	public class Response
		{
		public string response { set;get;}   //Esto devuelve el resultado de la Respuesta  OK || FAIL
		public string msg { set;get;} //Mensaje del Servicio
		public dynamic result { set;get;}
		}



    public class Saldos 
    {
        public Saldos() 
        {
            this.caja_principal = "0,00";
            this.casino = "0,00";
            this.betlaplus = "0,00";
            this.sportbook = "0,00";
            this.total = "0,00";
        }

        public Saldos(decimal caja_principal, decimal betlaplus, decimal casino, decimal sportbook) 
        {
            this.sportbook =  string.Format("{0:c}", sportbook);
            this.caja_principal = string.Format("{0:c}", caja_principal);
            this.betlaplus = string.Format("{0:c}", betlaplus);
            this.casino = string.Format("{0:c}", casino);
            this.total = string.Format("{0:c}", caja_principal + betlaplus + casino + sportbook);
        }


        public string caja_principal { set; get; }
        public string casino { set; get; }
        public string betlaplus { set; get; }
        public string sportbook { set; get; }
        public string total { set; get; }

    }
	}
