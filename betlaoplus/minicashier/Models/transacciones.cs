//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace minicashier.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class transacciones
    {
        public long id { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public Nullable<long> id_operacion { get; set; }
        public string desde { get; set; }
        public string hasta { get; set; }
        public Nullable<decimal> monto { get; set; }
        public Nullable<long> tipo { get; set; }
        public Nullable<decimal> caja { get; set; }
        public Nullable<decimal> casino { get; set; }
        public Nullable<decimal> sportbook { get; set; }
        public Nullable<decimal> saldo { get; set; }
        public string usuario { get; set; }
        public string banco { get; set; }
        public Nullable<long> operador { get; set; }
        public Nullable<decimal> poker { get; set; }
        public Nullable<decimal> saldodux { get; set; }
    }
}
