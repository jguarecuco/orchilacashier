﻿var serviceBalance = "/api/userbalance";
var serviceDeposito = "/api/userdeposit";
var serviceTransferencia = "/api/usertransferencia";
var serviceRetiro = "/api/userretirar";

var ExpreRegFecha = /(^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[/\\/](19|20)\d{2}$)/;
var ExpreRegDecimal = /^[0-9]+(\.[0-9]+)?$/;


function MessageStatus(status,transaccion) {
    
    var message = transaccion == 1 ? "El Deposito":
                  transaccion == 2 ? "La Transferencia" : "El Retiro";
    switch (status) {
        case 00:
            m = message + (transaccion == 1 || transaccion == 3) ? " se ha realizado " : " se ha realizada ";
            return m + "exitosamente!"
        break;
        case 01:
            m = message + (transaccion == 1 || transaccion == 3) ? "ha sido fallido" : "ha sido fallida";
            return m + ", su sesión ha caducado";
        break;

        default:
            return "DEFAULT!!";

    }
}


function updateSaldo(boolean) {

    if (boolean) {
       // $('div.info').css({ "display": "none" });
       // $('span.loading-amount').css({ "display": "inline-block" });
    }
    
    $.ajax({
        url: serviceBalance,
        method: "post",
        contentType: "application/json",
        data: JSON.stringify(window.token)
    }).done(function (data) {
        console.log("consulte saldos en el servidor...");
        console.log(data);
        $('span.loading-amount').css({ "display": "none" });
        $('div.info').css({ "display": "block" });
        $('strong[id=caja]').html(data.caja);
        $('strong[id=casino]').html(data.casino);
        $('strong[id=sportbook]').html(data.sportbook);
        $('strong[id=betlaoplus]').html(data.betlaoplus);
        $('strong[id=poker]').html(data.poker);
        $('strong[id=total]').html(data.total);

        
    }).fail(function (data) {
        Message(false, "Error al actualizar saldos");
        console.log("ERROR HERE --->");
        console.log(data);

        $('span.loading-amount').css({ "display": "none" });
        $('div.info').css({ "display": "block" });
        console.log("fallando al consultar saldos");
    });
}


/*MESSAGE WINDOW MODAL*/
function Message(all, msg) {
    $('div.modal-body').empty();

    if (all) {
        $('h4.modal-title').html("Transaccion realizada!");

        $('div.modal-body').append(msg);
        $('div.modal-body').append('<div id="icono"></div>');
        $('div#icono').addClass('icono-mensaje-check');
        $('#myModal').modal('show');
    } else {

        $('h4.modal-title').html("Transaccion fallida!");
        $('div.modal-body').append(msg);
        $('div.modal-body').append('<div id="icono"></div>');
        $('div#icono').addClass('icono-mensaje-cancel');
        $('#myModal').modal('show');
    }
}


$(window).load(function (){

    $('div.render').remove();
    //document.getElementsByClassName('render').remove;
});

$(document).on("ready", function () {

    /*Control de origen y destino de transferencia*/
    $('select#origen').on("change", function () {
        var option = $('select#origen option:selected').val();
        

        if (option == 1)
            $('select#destino').html("<option value=2>Casino</option><option value=3>Sportbook</option><option value=4>Betlaorchila Plus</option><option value=5>Poker</option>");
        else if (option == 2 || option == 3 || option == 4 || option == 5)
            $('select#destino').html("<option value=1>Caja Principal</option>");


        $('select#destino').selectpicker('refresh');
    });
    


    
    setInterval(function () {
        updateSaldo(false);
    }, 10000);
 
    


    //$('#bs-bottom-navbar-collapse').collapse('hide');
    //$('#bs-up-navbar-collapse-1').collapse('hide');

    $('html').not('nav#bs-bottom-navbar-collapse,nav#bs-up-navbar-collapse-1').contents().on("click", function () {
        $('.navbar-collapse.in').collapse('hide');
    });

    /*click a una opcion del navbar y que cierre automaticamente*/
    /*$("nav").find("li").on("click", "a", function () {
        $('.navbar-collapse.in').collapse('hide');
    });*/

    /*Ver saldo: abierto*/
    $("#bs-bottom-navbar-collapse").on("show.bs.collapse", function () {
        $('button#see-balance').removeClass("my-button-up");
        $('button#see-balance').addClass("my-button-up-open");
    });

    /*Ver saldo: cerrado*/
    $("#bs-bottom-navbar-collapse").on("hide.bs.collapse", function () {
        $('button#see-balance').removeClass("my-button-up-open");
        $('button#see-balance').addClass("my-button-up");
    });


    /*TAMANOS DE LA BASE DEL FONDO*/
    $('li > a, a#log-betla').on('click', function () {
        
        link = $(this).attr('href');
        var body = $('div.cont-general');

        if ($(this).attr('href') === "#bancos")
            body.css({ 'height': '450px', 'padding': '0' }); // 604px
        else
            body.css({ 'height': '100%', 'padding': '7% 0 7% 0' });
    });

    
    $('ul.navbar-nav li').click(function () {
        $('input[type=text]').not('.static').val("");
        $('input[type=text]').removeClass('input-error');
        $('input[type=text]').prop("placeholder", "");


        $('select#origen').html("<option value=1 selected>Caja Principal</option><option value=2>Casino</option><option value=3>Sportbook</option><option value=5>Poker</option>");
        $('select#destino').html("<option value=2 selected>Casino</option><option value=3>Sportbook</option><option value=5>Poker</option>");

        $('select#origen,select#destino').selectpicker('refresh');
    });


    /*validaciones ERRORES FORMULARIOS*/
    $("form#deposito,form#formtransf,form#retirar").on("submit", function (event) {
        event.preventDefault();
        
        var form = $(this);
        var error = 0;
        var patronAmount = new RegExp(ExpreRegDecimal);
        

        if ($(form).prop("id") == "deposito")
            var patronDate = new RegExp(ExpreRegFecha);

        
         
        $.each($(form).find('input[type=text]'), function (index,item) {
            
            if ($(this).val() == "") {
                error = 1; 
                $(this).addClass("input-error");
                $(this).prop("placeholder", "" + $(this).prop('name').toUpperCase() + " ES REQUERIDO");
                return false;
            }

            if ($(this).prop('name') == "monto") {
                

                if ($(this).val() <= 0){
                    error = 1;
                    $(this).addClass("input-error");
                    $(this).prop("placeholder", "MONTO DEBE DE SER POSITIVO");
                }
                else if (!patronAmount.test($(this).val())){
                    error = 1;
                    $(this).addClass("input-error");
                    Message(false, "El monto debe tener un formato valido!");
                }

                if (error == 1)
                    return false;
                
            }

            
            if (($(this).prop("name") === "fecha") && !patronDate.test($(this).val())) {
                error = 1;
                $(this).addClass("input-error");
                return false;
            }
            
        });
        
        if (error === 0) {

            var today = $('input#datemar').val().split('/')[2] + '-' + $('input#datemar').val().split('/')[1] + '-' + $('input#datemar').val().split('/')[0];

            var button = $(form).find('button[type=submit]')[0];
            var l = Ladda.create(button);

            if ($(form).prop('id') == "deposito") {
                var transaccion = { token: window.token, banco: $('select#banco option:selected').val(), n_transation: $('input#transaccion').val(), amount: $('input#monto').val(), fecha: today };
                l.start();

                $.ajax({
                    url: serviceDeposito,
                    method: 'POST',
                    data: transaccion
                }).done(function (data) {
                    console.log(data);
                    l.stop();
                    if (data.status == '00') {
                        $('select#banco option:selected').val("");
                        $('input#transaccion').val("");
                        $('input#monto').val("");
                        $('input#datemar').val("");

                        Message(true, data.Menssage);
                        updateSaldo(true);
                    } else {
                        l.stop();
                        Message(false, data.Menssage);
                    }

                }).fail(function (data) {
                    l.stop();
                    Message(false, "fallo al depositar");
                });



            }
            else if ($(form).prop('id') == "formtransf") {
                var transaccion = { token: window.token, origen: $('select#origen option:selected').val(), destino: $('select#destino option:selected').val(), monto: $('input#monto3').val() };

                l.start();
                $.ajax({
                    url: serviceTransferencia,
                    method: 'POST',
                    data: transaccion
                }).done(function (data) {
                    console.log(data);
                    l.stop();

                    if (data.status == '00'){
                        $('input#monto3').val("");
                        Message(true, data.Menssage);
                        updateSaldo(true);
                    }
                    else {
                        l.stop();
                        Message(false, data.Menssage);
                    }

                }).fail(function (data) {
                    l.stop();
                    Message(false, "Fallo al transferir");
                });
            }

            else if ($(form).prop('id') == "retirar") {
                var transaccion = { token: window.token, banco: $('select#banco2 option:selected').val(), tipo_cuenta: $('select#tipo_cuenta option:selected').val(), cedula: $('input#cedula2').val(), cuenta: $('input#cuenta2').val(), monto: $('input#monto2').val() };

                l.start();
                $.ajax({
                    url: serviceRetiro,
                    method: 'POST',
                    data: transaccion
                }).done(function (data) {
                    l.stop();

                    if (data.status == "00") {
                        $('select#banco2 option:selected').val("");
                        $('select#tipo_cuenta option:selected').val("");
                        $('input#cedula2').val("");
                        $('input#cuenta2').val("");
                        $('input#monto2').val("");
                        Message(true, data.Menssage);
                        updateSaldo(true);
                    } else {
                        l.stop();
                        Message(false, data.Menssage);
                    }

                }).fail(function (data) {
                    l.stop();
                    Message(false, "Fallo al retirar");
                });

            }
            
        }

    });

    $('input[type=text]').keydown(function () {

        if ($(this).hasClass("input-error") && $(this).val() != "") {
            $(this).removeClass("input-error");
            $(this).prop("placeholder", "")
        } else if ($(this).val() == "")
            $(this).addClass("input-error");

    });

    $('input[type=text]').change(function () {
        if ($(this).hasClass("input-error") && $(this).val() != "")
            $(this).removeClass("input-error");
        $(this).prop("placeholder", "")
    });



});