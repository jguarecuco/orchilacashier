﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using betlaoplus.Models;
namespace betlaoplus.Controllers
	{
	public class UserAddAccountController : ApiController
		{
		OrchilaPlusEntities db = new OrchilaPlusEntities( );
		public MDLResponse Post([FromBody] MDLNewUserAccount newuser)
			{
			MDLResponse rest = new MDLResponse( );
			
			var users = db.BPUsers.Where(f => f.Username == newuser.username || f.Email == newuser.email).ToList( );
			var _operator = db.BPOperators.Where(f => f.MasterKey == newuser.masterkey).ToList( );
			if (users.Count == 0)
				{
				BPUsers user_new = new BPUsers( );
				user_new.OperatorId = _operator.First( ).Id;
				user_new.Password = newuser.password;
				user_new.Username = newuser.username;
				user_new.Email = newuser.email;
				user_new.PGUserId=0;
				user_new.Status="A";
				db.BPUsers.Add(user_new);
				db.SaveChanges();

                BPCashier cajero = new BPCashier();
                cajero.AmountCashier = 0;
                cajero.OperatorId = user_new.OperatorId;
                cajero.Status = "A";
                cajero.UserId = user_new.Id;
                db.BPCashier.Add(cajero);
                db.SaveChanges();

                BPSessions session = new BPSessions( );
				session.Status = "A";
				session.Token = Utility.GetSign("123456" , user_new.Username + DateTime.Now.Ticks.ToString( ) + _operator.First( ).Id.ToString( ));
				session.Create_Date = DateTime.Now;
				session.UserId = user_new.Id;
				db.BPSessions.Add(session);
				db.SaveChanges( );
								 
				MDLResult result=new MDLResult();
				rest.RESULT=result;
				result.Username=user_new.Username;
				result.Token=session.Token;
				result.Msg="Ready";
				result.Id=user_new.Id;
				rest.Response="OK";

				}
			else
				{
				MDLResult result=new MDLResult();
				rest.RESULT=result;
				rest.Response="FAIL";
				rest.RESULT.Msg="Existe un usuario con la información registrada.";
				}
			
			return rest;

			}

		}
	}
