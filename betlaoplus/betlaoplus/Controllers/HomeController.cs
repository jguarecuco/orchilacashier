﻿using betlaoplus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace betlaoplus.Controllers
	{
	public class HomeController : Controller
		{
		//
		// GET: /Home/
		//public MDLResponse Post([FromBody] MDLBalanceRequest model)
        OrchilaPlusEntities db = new OrchilaPlusEntities(); 
		public String getBalance()
			{

			Dictionary<string , string> data = new Dictionary<string , string>( );

			data.Add("token" , Session["token"].ToString( ));
			data.Add("masterkey" , "BO08072015");
			string respuesta = Utility.HTTPPost(data , "http://localhost:8898/api/userbalance");

			MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);

			return "Saldo: "+ String.Format("{0:N}", rest.RESULT.UserBalance);
			}

		public ActionResult Index(string Id)
			{
		//	if (Session.Count == 0)
		//		return RedirectToAction("Login");
			Session.Add("token",Id);
			ViewBag.Balance = getBalance( );
            var session = db.BPSessions.Single(f => f.Token==Id);
            var usuario = db.BPUsers.Single(f => f.Id == session.UserId);
            MDLLogin model = new MDLLogin();
            model.username = usuario.Username;
            model.password = usuario.Password;
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("username", model.username);
            data.Add("password", model.password);
            data.Add("masterkey", "BO08072015");

            string respuesta = Utility.HTTPPost(data, "http://localhost:8898/api/userapi");
            MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);
            if (rest.Response == "OK")
            {
                Session["token"] = rest.RESULT.Token;
                Session["username"] = rest.RESULT.Username;
                Session["password"] = model.password;
                if (rest.RESULT.UserIdPG == 0)
                {

                    string respuesta1 = registerPG(rest.RESULT.Username.ToString(), model.password, "dumi", "dumi", "", "", "", "", "", "", "", "04120793371", "4001", new DateTime(1977, 2, 8));
                    var trest = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(respuesta1);
                    Session.Add("url", "http://deportes.betlaorchila.com/BLO/Login.aspx?account=" + trest.Account.ToString() + "&password=" + model.password);



                }

                return View();
            }
            else
            {
                ViewBag.Msg = rest.RESULT.Msg;
                return View();
            }


		//	return View( );


			}

        public void PGClearBalance()
        {

        }
		public string registerPG(string username,string password,string firstname,string lastname,string currency,string address,string city,string countrycode,string email,string languagecode,string nickname,string phone,string zip,DateTime birthday)
			{
			PgServiceSoap.ServiceSoapClient pginstant = new PgServiceSoap.ServiceSoapClient( );
			string fecha = birthday.Month.ToString( ) + "/" + birthday.Day.ToString( ) + "/" + birthday.Year.ToString( ) + " 12:00:00 AM";
            DateTime fd = DateTime.Parse(fecha);
			string sign = Utility.GetSign("i07:ffbV7u88R1l" , "7381B4BE-FB94-4CDA-913E-3BC8E661AD57" + username +
			  password +
			  firstname +
			   lastname +
			   "vef" +
				"direccion 1 aqui" +
				 "san jose" +
				  "ve" +
				   username+"@gmail.com" +
					"spa" +
					username +
					phone +
					zip + fecha);

			var resp = pginstant.AuthUser("7381B4BE-FB94-4CDA-913E-3BC8E661AD57" , sign , username ,  password  ,	firstname ,	  lastname ,
				   "vef" ,
					"direccion 1 aqui" ,
					 "san jose" ,
					  "ve" ,
					   username + "@gmail.com" ,
						"spa" ,
						username ,
						phone ,
						zip ,
						birthday);
            Session.Add("userpg", resp.Account);
       //   decimal monto=  getBalancePG(resp.Account, "vef", 0, "VSFH", 0);
           // depositPG(resp.Account, decimal.Parse("10000,25"), "VEF", DateTime.Now.Ticks, 0, 0, "", 0);
         //   withdrawPG(resp.Account, monto, "VEF", DateTime.Now.Ticks, 0, 0, "");
 /*
            string sign = Utility.GetSign("i07:ffbV7u88R1l", "7381B4BE-FB94-4CDA-913E-3BC8E661AD57" + "wolfie6"+
              "123456" +
             "frank" +
              "smith" +
              "vef" +
               "direccion 1 aqui" +
                "san jose" +
                 "ve" +
                  "wolfie@panoramagaming.com" +
                   "spa" +
                   "wolfie6" +
                   "123456" +
                   "123456" + fecha
            );

            var resp = pginstant.AuthUser("7381B4BE-FB94-4CDA-913E-3BC8E661AD57", sign,
                "wolfie6",
                 "123456",
                 "frank",
                  "smith",
                  "vef",
                   "direccion 1 aqui",
                    "san jose",
                     "ve",
                      "wolfie@panoramagaming.com",
                       "spa",
                       "wolfie6",
                       "123456",
                       "123456",
                        fd);
								*/

            /*
            string sign = Utility.GetSign("i07:ffbV7u88R1l", "7381B4BE-FB94-4CDA-913E-3BC8E661AD57" + Session["username"].ToString() +
			 Session["password"].ToString( ) +
			 "dumi"+
			  "dumi" +
			  "VEF" +
			   "DUMI" +
				"dumi" +
				 "VE" +
				  Session["username"].ToString( ) + "@gmail.com" +
				   "SP" +
				   Session["username"].ToString( ) +
                   "04120650835" +
				   "4001" +fecha
);

		var resp=	pginstant.AuthUser("7381B4BE-FB94-4CDA-913E-3BC8E661AD57" ,	sign  ,
			Session["username"].ToString( ) ,
			 Session["password"].ToString( ) ,
			 "dumi" ,
			  "dumi" ,
			  "VEF" ,
			   "DUMI" ,
				"dumi" ,
				 "VE" ,
				  Session["username"].ToString( ) + "@gmail.com" ,
				   "SP" ,
				   Session["username"].ToString( ) ,
                   "04120650835", 
				   "4001" ,
				   
					fd );
             */
			return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
			}
		public decimal  getBalancePG(string name,string currency,long SessionId,string gameModule,int type) {
		PgServiceSoap.ServiceSoapClient pginstant = new PgServiceSoap.ServiceSoapClient( );
        string sign = Utility.GetSign("i07:ffbV7u88R1l", "7381B4BE-FB94-4CDA-913E-3BC8E661AD57" + name +currency+SessionId.ToString()+gameModule+type.ToString()  );
        var resp = pginstant.GetBalance("7381B4BE-FB94-4CDA-913E-3BC8E661AD57", sign, name, currency, SessionId, gameModule, type); 
		
            return resp.Balance.Amount ;
		 }
        public void depositPG(string name,decimal amount, string currency,long reference, long SessionId, long gameroundid , string gameModule, int type)
        {
            PgServiceSoap.ServiceSoapClient pginstant = new PgServiceSoap.ServiceSoapClient();
            string sign = Utility.GetSign("i07:ffbV7u88R1l", "7381B4BE-FB94-4CDA-913E-3BC8E661AD57" + name+String.Format("{0:0.00}", amount ).Replace(",",".")   + currency +reference.ToString()  + SessionId.ToString()+gameroundid.ToString()  + gameModule + type.ToString());
            var resp = pginstant.Deposit ("7381B4BE-FB94-4CDA-913E-3BC8E661AD57", sign, name,amount , currency,reference , SessionId,gameroundid , gameModule, type); 
        }
        public void withdrawPG(string name, decimal amount, string currency, long reference, long SessionId, long gameroundid, string gameModule)
        {
            PgServiceSoap.ServiceSoapClient pginstant = new PgServiceSoap.ServiceSoapClient();
            string sign = Utility.GetSign("i07:ffbV7u88R1l", "7381B4BE-FB94-4CDA-913E-3BC8E661AD57" + name + String.Format("{0:0.00}", amount).Replace(",", ".") + currency + reference.ToString() + SessionId.ToString() + gameroundid.ToString() + gameModule );
            var resp = pginstant.Withdraw ("7381B4BE-FB94-4CDA-913E-3BC8E661AD57", sign, name, amount, currency, reference, SessionId, gameroundid, gameModule);
        }
		[HttpPost( )]

		public ActionResult Registro(MDLNewUserAccount model)
			{
			Dictionary<string , string> data = new Dictionary<string , string>( );
			data.Add("username" , model.username);
			data.Add("password" , model.password);
			data.Add("email" , model.email);
			data.Add("masterkey" , "BO08072015");
			string respuesta = Utility.HTTPPost(data , "http://localhost:8898/api/useraddaccount");
			MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);

			if (rest.Response == "OK")
				{
				Session["token"] = rest.RESULT.Token;
				Session["username"] = rest.RESULT.Username;

				return RedirectToAction("index" , new { id = rest.RESULT.Token });
				}
			else
				{
				ViewBag.Msg = rest.RESULT.Msg;
				return View( );
				}
			}
		public ActionResult Retirar()
			{
			return View( );
			}

		[HttpPost( )]
		public ActionResult Retirar(MDLTransaction model)
			{

			Dictionary<string , string> data = new Dictionary<string , string>( );
			data.Add("amount_transaction" , model.amount_transaction.ToString( ));
			data.Add("token" , Session["token"].ToString( ));
			data.Add("masterkey" , "BO08072015");
			// Withdraw = 111
			data.Add("type_transaction" , "111");

			string respuesta = Utility.HTTPPost(data , "http://localhost:8898/api/userwithdraw");
			MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);

			if (rest.Response == "OK")
				{

				return RedirectToAction("index" , new { id = Session["token"].ToString( ) });
				}
			else
				{
				ViewBag.Msg = rest.RESULT.Msg;
				return View( );
				}
			}

		public ActionResult Deposito()
			{
			return View( );
			}

		[HttpPost( )]
		public ActionResult Deposito(MDLTransaction model)
			{
			Dictionary<string , string> data = new Dictionary<string , string>( );
			data.Add("amount_transaction" , model.amount_transaction.ToString( ));
			data.Add("token" , Session["token"].ToString( ));
			data.Add("masterkey" , "BO08072015");
			//if(model.type_transaction == "110")
			data.Add("type_transaction" , "110"); // 110

			string respuesta = Utility.HTTPPost(data , "http://localhost:8898/api/userdeposit");
			MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);

			if (rest.Response == "OK")
				{

				return RedirectToAction("index" , new { id = Session["token"].ToString( ) });
				}
			else
				{
				ViewBag.Msg = rest.RESULT.Msg;
				return View( );
				}
			}


		public ActionResult Registro()
			{
			return View( );
			}

		public ActionResult Error()
			{
			return View( );
			}
		public ActionResult Test()
			{
			return View( );
			}
		[HttpPost( )]
		public ActionResult Login(MDLLogin model)
			{
			Dictionary<string , string> data = new Dictionary<string , string>( );
			data.Add("username" , model.username);
			data.Add("password" , model.password);
			data.Add("masterkey" , "BO08072015");
			string respuesta = Utility.HTTPPost(data , "http://localhost:8898/api/userapi");
			MDLResponse rest = Newtonsoft.Json.JsonConvert.DeserializeObject<MDLResponse>(respuesta);
			if (rest.Response == "OK")
				{
				Session["token"] = rest.RESULT.Token;
				Session["username"] = rest.RESULT.Username;
				Session["password"] = model.password;
                if (rest.RESULT.UserIdPG == 0) {
                    
                    string respuesta1= registerPG(rest.RESULT.Username.ToString(),model.password,"dumi","dumi","","","","","","","","04120793371","4001",new DateTime(1977,2,8));
					var trest=Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(respuesta1);
					Session.Add("url", "http://deportes.betlaorchila.com/BLO/Login.aspx?account=" + trest.Account.ToString() + "&password=" + model.password);
                }

				return RedirectToAction("index" , new { id = rest.RESULT.Token });
				}
			else
				{
				ViewBag.Msg = rest.RESULT.Msg;
				return View( );
				}

			}

        public ActionResult Page3()
        {
            return View();
        }
        public ActionResult Page2() {
            return View();
        }
        public ActionResult Page1() {
        //    Session.Add("url", "http://localhost:8898/home/page1");
            return View();
        }
		public ActionResult Login()
			{
			ViewBag.Msg = "";
			return View( );
			}
		public ActionResult Recordar()
			{
			emailBienvenida("jguarecuco@datawebonline.net" , "Bienvenido mis niños");
			return View( );
			}

        public ActionResult PGView()
        {
            return Redirect(Session["url"].ToString() );
        }
		public string emailBienvenida(string email1 , string mensaje)
			{
			string nombre = "Betlaorchila.com";
			string email = "registro@betlaorchila.com";

			string NombreEnvio = "BIENVENIDO A NUESTRA CASA DE JUEGO ONLINE";
			string tomail = email1;
			System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage( );
			correo.To.Add(tomail);
			correo.From = new System.Net.Mail.MailAddress(email , NombreEnvio , System.Text.Encoding.UTF8);
			correo.Subject = NombreEnvio;
			correo.SubjectEncoding = System.Text.Encoding.UTF8;
			correo.Body = mensaje + " \n Email Contacto: " + email;
			correo.BodyEncoding = System.Text.Encoding.UTF8;
			correo.IsBodyHtml = true;
			correo.Priority = System.Net.Mail.MailPriority.Normal;
			System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient( );
			smtp.Port = 25;
			smtp.Host = "198.50.96.138";

			try
				{
				smtp.Send(correo);
				}
			catch (Exception ex)
				{
				var g = ex.Message;
				return "Error";
				}
			return "Mensaje enviado Satisfactoriamente.";
			}
		}
	}
