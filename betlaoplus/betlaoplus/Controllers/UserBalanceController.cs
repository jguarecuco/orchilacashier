﻿using betlaoplus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace betlaoplus.Controllers
	{
	public class UserBalanceController : ApiController
		{
		OrchilaPlusEntities db = new OrchilaPlusEntities( );
		 
		public MDLResponse Post([FromBody] MDLBalanceRequest model)
			{
			try
				{
			 
				var user = db.BPSessions.Single(f => f.Token == model.token);
				long userid=user.UserId.Value;
				var balance=db.BPCashier.Single(f=>f.UserId==userid);
				MDLBalanceResponse respuesta = new MDLBalanceResponse( );
				respuesta.UserId = 1;
				respuesta.UserBalance = balance.AmountCashier.Value;
				respuesta.Status = "A";
				MDLResponse rest = new MDLResponse( );
				rest.Response = "OK";
				rest.RESULT = respuesta;
				return rest;
				}
			catch (Exception ex)
				{
				MDLBalanceResponse respuesta = new MDLBalanceResponse( );
				respuesta.Msg = "Error en sessión";
				MDLResponse rest = new MDLResponse( );
				rest.Response = "FAIL";
				rest.RESULT = respuesta;
				return rest;
				}

			}
		}
	}
