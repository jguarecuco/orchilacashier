﻿using betlaoplus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace betlaoplus.Controllers
{
    public class UserExistApiController : ApiController
    {
	OrchilaPlusEntities db = new OrchilaPlusEntities( );
	public MDLResponse Post([FromBody] MDLUsers model)
		{

		var response = new MDLResponse( );
		response.RESULT = new MDLResult( );

		var _operator = db.BPOperators.Where(f => f.MasterKey == model.masterkey).ToList( );

		if (_operator.Count == 0)
			{
			response.Response = "FAIL";
			response.RESULT.Msg = "No tiene acceso disponible";
			return response;
			}
		
		long operatorid = _operator[0].Id;
		try
			{

			var _user = db.BPUsers.Where(f => f.OperatorId == operatorid & f.Username == model.username).ToList( ).First( );
			if (_user.Password == model.password)
				{

				response.Response = "OK";
				response.RESULT.Username = _user.Username;
				response.RESULT.Msg = ":P";

				}
			else
				{
				response.Response = "FAIL";
				response.RESULT.Msg = "Usuario y contraseña no valida.";
				}

			return response;
			}
		catch (Exception ex)
			{
			response.Response = "FAIL";
			response.RESULT.Msg = "Usuario y contraseña no valida.";
			return response;
			}
		}
    }
}
