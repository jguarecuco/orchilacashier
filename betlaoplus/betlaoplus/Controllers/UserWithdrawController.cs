﻿using betlaoplus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace betlaoplus.Controllers
{
    public class UserWithdrawController : ApiController
    {
        OrchilaPlusEntities db = new OrchilaPlusEntities( );
		public MDLResponse Post([FromBody] MDLTransaction model)
			{
			try
				{
				if (model.amount_transaction < 0)
					{
					MDLDepositResponse respuesta1 = new MDLDepositResponse( );
					respuesta1.Msg = "Monto a retirar debe ser mayor a 0";
					MDLResponse rest1 = new MDLResponse( );
					rest1.Response = "FAIL";
					rest1.RESULT = respuesta1;
					return rest1;
					}
				var user = db.BPSessions.Single(f => f.Token == model.token);
				long userid = user.UserId.Value;
				var balance = db.BPCashier.Single(f => f.UserId == userid);
                if (balance.AmountCashier < model.amount_transaction) {
                    MDLDepositResponse respuesta1 = new MDLDepositResponse();
                    respuesta1.Msg = "Monto a retirar debe ser menor del que tiene en caja";
                    MDLResponse rest1 = new MDLResponse();
                    rest1.Response = "FAIL";
                    rest1.RESULT = respuesta1;
                    return rest1;
                }
				balance.AmountCashier-=model.amount_transaction;
				db.Entry(balance).State=System.Data.Entity.EntityState.Modified;
				db.SaveChanges();
				BPTransation asiento=new BPTransation();
				asiento.IntegrationId=0;
				asiento.UserId=userid;
				asiento.Description="Retiro de cuenta";
				asiento.OperatorId=balance.OperatorId;
				asiento.DateTransation=DateTime.Now;
				asiento.AmountTransaction=model.amount_transaction;
				asiento.Status="A";
				asiento.TypeTransaction = model.type_transaction;
				db.BPTransation.Add(asiento);
				db.SaveChanges();

				MDLDepositResponse respuesta=new MDLDepositResponse();
				respuesta.Msg="Success";
 
				MDLResponse rest = new MDLResponse( );
				rest.Response = "OK";
				rest.RESULT = respuesta;
				return rest;
				}
			catch (Exception ex)
				{
				MDLDepositResponse respuesta = new MDLDepositResponse( );
				respuesta.Msg = "Error en datos de sesión";
				MDLResponse rest = new MDLResponse( );
				rest.Response = "FAIL";
				rest.RESULT = respuesta;
				return rest;
				}
			}
			
    
    }
}
