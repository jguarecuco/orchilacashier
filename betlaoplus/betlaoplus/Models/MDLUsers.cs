﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace betlaoplus.Models
	{
	public class MDLUsers
		{
		public string username { set; get; }
		public string password { set; get; }
		public string masterkey { set; get; }
		}
	public class MDLBalanceRequest
		{
		public string token { set; get; }
		public string masterkey { set; get; }

		}
	public class MDLBalanceResponse
		{
		public long UserId { set; get; }
		public decimal UserBalance { set; get; }
		public string Status { set; get; }
		public string Msg { set; get; }

		}
	public class MDLNewUserAccount
		{
		public string masterkey { set; get; }
		public string username { set;get;}
		public string email { set;get;}
		public string password { set;get;}
 
		}
	}