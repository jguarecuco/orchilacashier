﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace betlaoplus.Models
	{
	public class MDLResponse
		{
		public string Response { set; get; } //OK OR FAIL 
		public dynamic RESULT { set; get; }
		}
	public class MDLResult
		{
		public string Msg { set; get; }
		public long Id { set; get; }
		public string Token { set; get; }
		public string Username { set; get; }
        public long UserIdPG { set; get; }
		}
	 
	public class MDLLogin
		{
		public string username { set; get; }
		public string password { set; get; }
		}
	}
